# README #

Welcome to RatingTracker.  RatingTracker began as a fun hobby-project that I created to track competitive ratings for foosball games between the devs in our office.  This simple app uses the Elo rating system to rank players in head-to-head competition.  For more information about Elo, please see https://en.wikipedia.org/wiki/Elo_rating_system 

### What is this repository for? ###

Leveraging many of the built-in capabilities in the ASP.NET MVC stack, this project also serves as was a quick way to demonstrate use of the framework and patterns for:

* user authentication via Microsoft.AspNet.Identity
* 3rd party authentication via OAuth (Twitter)
* responsive layout with Twitter Bootstrap
* simple charting with c3.js
* managing database source code in a visual studio sql server database project
* separation of concerns across models, views, controllers, query objects, and services
* use of LLBL Gen Pro as an ORM layer, alternative to Entity Framework
* simple console application to generate sample data set for testing and demos

Currently RatingTracker is in Beta status - code comes as-is with no warranties.

View a live hosted instance of RatingTracker at http://ratingtracker.azurewebsites.net/