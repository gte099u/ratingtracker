﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RatingTracker.Application
{
    public class EloCalculator
    {
        private const int K = 24;
        public const int DEFAULT_RATING = 1200;

        public static EloResult CalculateElo(short winnerRating, short loserRating)
        {
            var ratingDiff = loserRating - winnerRating;

            var pct = 1 / (1 + Math.Pow(10, (double)ratingDiff / 400));

            var winnerSwing = (short)Math.Round(K * (1 - pct), 0);
            var loserSwing = (short)Math.Round(K * (1 - pct), 0);

            return new EloResult
            {
                Bias = pct,
                WinnerRating = (short)(winnerRating + winnerSwing),
                LoserRating = (short)(loserRating - loserSwing)
            };
        }

        public static double GetBias(short player1Rating, short player2Rating)
        {
            var ratingDiff = player1Rating - player2Rating;

            return 1 / (1 + Math.Pow(10, (double)ratingDiff / 400));
        }
    }
}