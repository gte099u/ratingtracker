﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RatingTracker.Application
{
    public class EloResult
    {
        public double Bias { get; set; }
        public short WinnerRating { get; set; }
        public short LoserRating { get; set; }
    }
}
