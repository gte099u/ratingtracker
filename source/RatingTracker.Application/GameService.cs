﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class GameService
    {
        public void Create(int winnerId, int loserId)
        {
            Create(winnerId, loserId, DateTime.Now);
        }

        public void Create(int winnerId, int loserId, DateTime timestamp)
        {
            if (winnerId == loserId)
                throw new Exception("Winner and loser must be different players.");

            var service = new RatingCalculationService();

            using (var adapter = new DataAccessAdapter())
            {
                var winner = new LeaguePlayerEntity(winnerId);
                var loser = new LeaguePlayerEntity(loserId);

                adapter.FetchEntity(winner);
                adapter.FetchEntity(loser);

                if (winner.LeagueId != loser.LeagueId)
                    throw new Exception("Games must be between players in the same league");

                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "New Game");

                    // save the game
                    var toSave = new GameEntity
                    {
                        WinnerId = winnerId,
                        LoserId = loserId,
                        TimeStamp = timestamp
                    };

                    adapter.SaveEntity(toSave);

                    // calculate new ratings
                    var game = new GameEntity(toSave.Id);
                    var path = new PrefetchPath2(EntityType.GameEntity)
                    {
                        GameEntity.PrefetchPathLoser,
                        GameEntity.PrefetchPathWinner
                    };

                    adapter.FetchEntity(game, path);
                    service.ApplyGameResults(game, adapter);

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }
        }
        public void Delete(int gameId)
        {
            var service = new RatingCalculationService();

            using (var adapter = new DataAccessAdapter())
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "New Game");

                    // drop all post-game rating stats
                    adapter.DeleteEntitiesDirectly(typeof(PostGameRatingEntity), null);

                    // delete the game
                    var toDelete = new GameEntity(gameId);
                    adapter.DeleteEntity(toDelete);

                    service.RecalculateAllRatings(adapter);

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }
        }
    }
}
