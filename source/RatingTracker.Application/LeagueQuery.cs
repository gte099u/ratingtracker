﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class LeagueQuery
    {
        public IEnumerable<LeagueEntity> GetAll()
        {
            using (var adapter = new DataAccessAdapter())
            {
                var allLeagues = new EntityCollection<LeagueEntity>();
                adapter.FetchEntityCollection(allLeagues, null);
                return allLeagues.OrderBy(m => m.Name);
            }
        }
    }
}
