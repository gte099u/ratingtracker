﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class LeagueService
    {
        public void AddPlayer(int leagueId, int playerId)
        {
            using (var adapter = new DataAccessAdapter())
            {
                try
                {
                    adapter.StartTransaction(System.Data.IsolationLevel.ReadCommitted, "add_player");

                    var toSave = new LeaguePlayerEntity
                    {
                        LeagueId = leagueId,
                        PlayerId = playerId,
                        CurrentRating = EloCalculator.DEFAULT_RATING
                    };

                    adapter.SaveEntity(toSave);

                    var calcService = new RatingCalculationService();
                    calcService.RecalculateAllRatings(adapter);

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }
        }

        public void Create(string name)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var toSave = new LeagueEntity
                {
                    Name = name
                };
                adapter.SaveEntity(toSave);
            }
        }

        public void RemovePlayer(int toRemoveId)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var toDelete = new LeaguePlayerEntity(toRemoveId);
                adapter.DeleteEntity(toDelete);
            }
        }
    }
}
