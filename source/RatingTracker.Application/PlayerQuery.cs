﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class PlayerQuery
    {
        public IEnumerable<PlayerEntity> GetAll()
        {
            using (var adapter = new DataAccessAdapter())
            {
                var allPlayers = new EntityCollection<PlayerEntity>();
                adapter.FetchEntityCollection(allPlayers, null);
                return allPlayers.OrderByDescending(m => m.Name);
            }
        }

        public IEnumerable<LeaguePlayerDetailEntity> GetPlayersForLeague(int leagueId)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var players = new EntityCollection<LeaguePlayerDetailEntity>();

                var playerFilter = new RelationPredicateBucket(LeaguePlayerDetailFields.LeagueId == leagueId);
                adapter.FetchEntityCollection(players, playerFilter);

                return players;
            }
        }

        public IEnumerable<LeaguePlayerDetailEntity> GetAllLeaguePlayers()
        {
            using (var adapter = new DataAccessAdapter())
            {
                var players = new EntityCollection<LeaguePlayerDetailEntity>();

                adapter.FetchEntityCollection(players, null);

                return players;
            }
        }

        public PlayerEntity Get(int id)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var player = new PlayerEntity(id);
                adapter.FetchEntity(player);
                return player;
            }
        }
    }
}
