﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class PlayerService
    {
        public void Create(string name)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var toSave = new PlayerEntity
                {
                    Name = name
                };
                adapter.SaveEntity(toSave);
            }
        }

        public void Save(PlayerEntity toSave)
        {
            using (var adapter = new DataAccessAdapter())
            {               
                adapter.SaveEntity(toSave);
            }
        }
    }
}
