﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.Application
{
    public class RatingCalculationService
    {
        public void RecalculateAllRatings()
        {
            using (var adapter = new DataAccessAdapter())
            {
                try
                {
                    adapter.StartTransaction(IsolationLevel.ReadCommitted, "New Game");

                    RecalculateAllRatings(adapter);

                    adapter.Commit();
                }
                catch (Exception)
                {
                    adapter.Rollback();
                    throw;
                }
            }
        }
        
        internal void RecalculateAllRatings(IDataAccessAdapter adapter)
        {
            // drop all post-game rating stats
            adapter.DeleteEntitiesDirectly(typeof(PostGameRatingEntity), null);

            // reset all ratings to default rating
            var leaguePlayer = new LeaguePlayerEntity { CurrentRating = EloCalculator.DEFAULT_RATING };
            adapter.UpdateEntitiesDirectly(leaguePlayer, null);            

            // calculate new ratings

            var allGames = new EntityCollection<GameEntity>();          

            adapter.FetchEntityCollection(allGames, null);

            foreach (var game in allGames.OrderBy(m => m.TimeStamp))
            {
                ApplyGameResults(game, adapter);
            }
        }

        public void ApplyGameResults(GameEntity game, IDataAccessAdapter adapter)
        {
            // obtain players current ratings
            var winner = new LeaguePlayerEntity(game.WinnerId);
            var loser = new LeaguePlayerEntity(game.LoserId);
            adapter.FetchEntity(winner);
            adapter.FetchEntity(loser);

            game.WinnerOriginalRating = winner.CurrentRating;
            game.LoserOriginalRating = loser.CurrentRating;

            // calculate Elo result
            var result = EloCalculator.CalculateElo(game.WinnerOriginalRating, game.LoserOriginalRating);

            // update game details
            game.Bias = result.Bias;
            winner.CurrentRating = result.WinnerRating;
            loser.CurrentRating = result.LoserRating;

            // save the game
            adapter.SaveEntity(game);

            // save player records
            adapter.SaveEntity(winner);
            adapter.SaveEntity(loser);

            // index current post-game ratings for league players
            var allPlayers = new EntityCollection<LeaguePlayerEntity>();
            var leaguePlayersFilter = new RelationPredicateBucket(LeaguePlayerFields.LeagueId == winner.LeagueId);
            adapter.FetchEntityCollection(allPlayers, leaguePlayersFilter);

            foreach (var player in allPlayers)
            {
                var toSave = new PostGameRatingEntity
                {
                    GameId = game.Id,
                    LeaguePlayerId = player.Id,
                    Rating = player.CurrentRating
                };

                adapter.SaveEntity(toSave);
            }
        }
    }
}
