﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.EntityClasses
{
    public partial class LeagueGameDetailEntity
    {
        public bool IsUpset
        {
            get
            {
                return Bias <= .4;
            }
        }

        public bool IsCriticalUpset
        {
            get
            {
                return Bias <= .2;
            }
        }
    }
}
