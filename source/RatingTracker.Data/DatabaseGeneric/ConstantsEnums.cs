﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;

namespace RatingTracker
{
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Game.</summary>
	public enum GameFieldIndex
	{
		///<summary>Bias. </summary>
		Bias,
		///<summary>Id. </summary>
		Id,
		///<summary>LoserId. </summary>
		LoserId,
		///<summary>LoserOriginalRating. </summary>
		LoserOriginalRating,
		///<summary>TimeStamp. </summary>
		TimeStamp,
		///<summary>WinnerId. </summary>
		WinnerId,
		///<summary>WinnerOriginalRating. </summary>
		WinnerOriginalRating,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: League.</summary>
	public enum LeagueFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LeagueGameDetail.</summary>
	public enum LeagueGameDetailFieldIndex
	{
		///<summary>Bias. </summary>
		Bias,
		///<summary>GameId. </summary>
		GameId,
		///<summary>LeagueId. </summary>
		LeagueId,
		///<summary>LoserId. </summary>
		LoserId,
		///<summary>LoserName. </summary>
		LoserName,
		///<summary>LoserOriginalRating. </summary>
		LoserOriginalRating,
		///<summary>LoserPlayerId. </summary>
		LoserPlayerId,
		///<summary>TimeStamp. </summary>
		TimeStamp,
		///<summary>WinnerId. </summary>
		WinnerId,
		///<summary>WinnerName. </summary>
		WinnerName,
		///<summary>WinnerOriginalRating. </summary>
		WinnerOriginalRating,
		///<summary>WinnerPlayerId. </summary>
		WinnerPlayerId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LeaguePlayer.</summary>
	public enum LeaguePlayerFieldIndex
	{
		///<summary>CurrentRating. </summary>
		CurrentRating,
		///<summary>Id. </summary>
		Id,
		///<summary>LeagueId. </summary>
		LeagueId,
		///<summary>PlayerId. </summary>
		PlayerId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: LeaguePlayerDetail.</summary>
	public enum LeaguePlayerDetailFieldIndex
	{
		///<summary>CurrentRating. </summary>
		CurrentRating,
		///<summary>LeagueId. </summary>
		LeagueId,
		///<summary>LeagueName. </summary>
		LeagueName,
		///<summary>LeaguePlayerId. </summary>
		LeaguePlayerId,
		///<summary>Name. </summary>
		Name,
		///<summary>PlayerId. </summary>
		PlayerId,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: Player.</summary>
	public enum PlayerFieldIndex
	{
		///<summary>Id. </summary>
		Id,
		///<summary>Name. </summary>
		Name,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PostGameRating.</summary>
	public enum PostGameRatingFieldIndex
	{
		///<summary>GameId. </summary>
		GameId,
		///<summary>Id. </summary>
		Id,
		///<summary>LeaguePlayerId. </summary>
		LeaguePlayerId,
		///<summary>Rating. </summary>
		Rating,
		/// <summary></summary>
		AmountOfFields
	}
	/// <summary>Index enum to fast-access EntityFields in the IEntityFields collection for the entity: PostGameRatingDetail.</summary>
	public enum PostGameRatingDetailFieldIndex
	{
		///<summary>GameId. </summary>
		GameId,
		///<summary>LeagueId. </summary>
		LeagueId,
		///<summary>LeagueName. </summary>
		LeagueName,
		///<summary>LeaguePlayerId. </summary>
		LeaguePlayerId,
		///<summary>PlayerId. </summary>
		PlayerId,
		///<summary>PlayerName. </summary>
		PlayerName,
		///<summary>Rating. </summary>
		Rating,
		///<summary>TimeStamp. </summary>
		TimeStamp,
		/// <summary></summary>
		AmountOfFields
	}



	/// <summary>Enum definition for all the entity types defined in this namespace. Used by the entityfields factory.</summary>
	public enum EntityType
	{
		///<summary>Game</summary>
		GameEntity,
		///<summary>League</summary>
		LeagueEntity,
		///<summary>LeagueGameDetail</summary>
		LeagueGameDetailEntity,
		///<summary>LeaguePlayer</summary>
		LeaguePlayerEntity,
		///<summary>LeaguePlayerDetail</summary>
		LeaguePlayerDetailEntity,
		///<summary>Player</summary>
		PlayerEntity,
		///<summary>PostGameRating</summary>
		PostGameRatingEntity,
		///<summary>PostGameRatingDetail</summary>
		PostGameRatingDetailEntity
	}


	#region Custom ConstantsEnums Code
	
	// __LLBLGENPRO_USER_CODE_REGION_START CustomUserConstants
	// __LLBLGENPRO_USER_CODE_REGION_END
	#endregion

	#region Included code

	#endregion
}

