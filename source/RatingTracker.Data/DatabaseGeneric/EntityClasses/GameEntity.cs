﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using RatingTracker;
using RatingTracker.HelperClasses;
using RatingTracker.FactoryClasses;
using RatingTracker.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'Game'.<br/><br/></summary>
	[Serializable]
	public partial class GameEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<PostGameRatingEntity> _postGameRatings;
		private LeaguePlayerEntity _loser;
		private LeaguePlayerEntity _winner;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name Loser</summary>
			public static readonly string Loser = "Loser";
			/// <summary>Member name Winner</summary>
			public static readonly string Winner = "Winner";
			/// <summary>Member name PostGameRatings</summary>
			public static readonly string PostGameRatings = "PostGameRatings";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static GameEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public GameEntity():base("GameEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public GameEntity(IEntityFields2 fields):base("GameEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this GameEntity</param>
		public GameEntity(IValidator validator):base("GameEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="id">PK value for Game which data should be fetched into this Game object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GameEntity(System.Int32 id):base("GameEntity")
		{
			InitClassEmpty(null, null);
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for Game which data should be fetched into this Game object</param>
		/// <param name="validator">The custom validator object for this GameEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public GameEntity(System.Int32 id, IValidator validator):base("GameEntity")
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected GameEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_postGameRatings = (EntityCollection<PostGameRatingEntity>)info.GetValue("_postGameRatings", typeof(EntityCollection<PostGameRatingEntity>));
				_loser = (LeaguePlayerEntity)info.GetValue("_loser", typeof(LeaguePlayerEntity));
				if(_loser!=null)
				{
					_loser.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_winner = (LeaguePlayerEntity)info.GetValue("_winner", typeof(LeaguePlayerEntity));
				if(_winner!=null)
				{
					_winner.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((GameFieldIndex)fieldIndex)
			{
				case GameFieldIndex.LoserId:
					DesetupSyncLoser(true, false);
					break;
				case GameFieldIndex.WinnerId:
					DesetupSyncWinner(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "Loser":
					this.Loser = (LeaguePlayerEntity)entity;
					break;
				case "Winner":
					this.Winner = (LeaguePlayerEntity)entity;
					break;
				case "PostGameRatings":
					this.PostGameRatings.Add((PostGameRatingEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "Loser":
					toReturn.Add(Relations.LeaguePlayerEntityUsingLoserId);
					break;
				case "Winner":
					toReturn.Add(Relations.LeaguePlayerEntityUsingWinnerId);
					break;
				case "PostGameRatings":
					toReturn.Add(Relations.PostGameRatingEntityUsingGameId);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "Loser":
					SetupSyncLoser(relatedEntity);
					break;
				case "Winner":
					SetupSyncWinner(relatedEntity);
					break;
				case "PostGameRatings":
					this.PostGameRatings.Add((PostGameRatingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "Loser":
					DesetupSyncLoser(false, true);
					break;
				case "Winner":
					DesetupSyncWinner(false, true);
					break;
				case "PostGameRatings":
					this.PerformRelatedEntityRemoval(this.PostGameRatings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_loser!=null)
			{
				toReturn.Add(_loser);
			}
			if(_winner!=null)
			{
				toReturn.Add(_winner);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.PostGameRatings);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_postGameRatings", ((_postGameRatings!=null) && (_postGameRatings.Count>0) && !this.MarkedForDeletion)?_postGameRatings:null);
				info.AddValue("_loser", (!this.MarkedForDeletion?_loser:null));
				info.AddValue("_winner", (!this.MarkedForDeletion?_winner:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new GameRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PostGameRating' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPostGameRatings()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(PostGameRatingFields.GameId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'LeaguePlayer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoLoser()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(LeaguePlayerFields.Id, null, ComparisonOperator.Equal, this.LoserId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'LeaguePlayer' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoWinner()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(LeaguePlayerFields.Id, null, ComparisonOperator.Equal, this.WinnerId));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._postGameRatings);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._postGameRatings = (EntityCollection<PostGameRatingEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._postGameRatings != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<PostGameRatingEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PostGameRatingEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("Loser", _loser);
			toReturn.Add("Winner", _winner);
			toReturn.Add("PostGameRatings", _postGameRatings);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Bias", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoserId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LoserOriginalRating", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("TimeStamp", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WinnerId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("WinnerOriginalRating", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _loser</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLoser(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _loser, new PropertyChangedEventHandler( OnLoserPropertyChanged ), "Loser", RatingTracker.RelationClasses.StaticGameRelations.LeaguePlayerEntityUsingLoserIdStatic, true, signalRelatedEntity, "Games", resetFKFields, new int[] { (int)GameFieldIndex.LoserId } );
			_loser = null;
		}

		/// <summary> setups the sync logic for member _loser</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLoser(IEntityCore relatedEntity)
		{
			if(_loser!=relatedEntity)
			{
				DesetupSyncLoser(true, true);
				_loser = (LeaguePlayerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _loser, new PropertyChangedEventHandler( OnLoserPropertyChanged ), "Loser", RatingTracker.RelationClasses.StaticGameRelations.LeaguePlayerEntityUsingLoserIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLoserPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _winner</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncWinner(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _winner, new PropertyChangedEventHandler( OnWinnerPropertyChanged ), "Winner", RatingTracker.RelationClasses.StaticGameRelations.LeaguePlayerEntityUsingWinnerIdStatic, true, signalRelatedEntity, "Games1", resetFKFields, new int[] { (int)GameFieldIndex.WinnerId } );
			_winner = null;
		}

		/// <summary> setups the sync logic for member _winner</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncWinner(IEntityCore relatedEntity)
		{
			if(_winner!=relatedEntity)
			{
				DesetupSyncWinner(true, true);
				_winner = (LeaguePlayerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _winner, new PropertyChangedEventHandler( OnWinnerPropertyChanged ), "Winner", RatingTracker.RelationClasses.StaticGameRelations.LeaguePlayerEntityUsingWinnerIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnWinnerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this GameEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static GameRelations Relations
		{
			get	{ return new GameRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PostGameRating' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPostGameRatings
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<PostGameRatingEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PostGameRatingEntityFactory))), (IEntityRelation)GetRelationsForField("PostGameRatings")[0], (int)RatingTracker.EntityType.GameEntity, (int)RatingTracker.EntityType.PostGameRatingEntity, 0, null, null, null, null, "PostGameRatings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'LeaguePlayer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathLoser
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(LeaguePlayerEntityFactory))),	(IEntityRelation)GetRelationsForField("Loser")[0], (int)RatingTracker.EntityType.GameEntity, (int)RatingTracker.EntityType.LeaguePlayerEntity, 0, null, null, null, null, "Loser", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'LeaguePlayer' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathWinner
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(LeaguePlayerEntityFactory))),	(IEntityRelation)GetRelationsForField("Winner")[0], (int)RatingTracker.EntityType.GameEntity, (int)RatingTracker.EntityType.LeaguePlayerEntity, 0, null, null, null, null, "Winner", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The Bias property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."Bias"<br/>
		/// Table field type characteristics (type, precision, scale, length): Float, 38, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Double Bias
		{
			get { return (System.Double)GetValue((int)GameFieldIndex.Bias, true); }
			set	{ SetValue((int)GameFieldIndex.Bias, value); }
		}

		/// <summary> The Id property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)GameFieldIndex.Id, true); }
			set	{ SetValue((int)GameFieldIndex.Id, value); }
		}

		/// <summary> The LoserId property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."LoserId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LoserId
		{
			get { return (System.Int32)GetValue((int)GameFieldIndex.LoserId, true); }
			set	{ SetValue((int)GameFieldIndex.LoserId, value); }
		}

		/// <summary> The LoserOriginalRating property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."LoserOriginalRating"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 LoserOriginalRating
		{
			get { return (System.Int16)GetValue((int)GameFieldIndex.LoserOriginalRating, true); }
			set	{ SetValue((int)GameFieldIndex.LoserOriginalRating, value); }
		}

		/// <summary> The TimeStamp property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."TimeStamp"<br/>
		/// Table field type characteristics (type, precision, scale, length): DateTime, 0, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.DateTime TimeStamp
		{
			get { return (System.DateTime)GetValue((int)GameFieldIndex.TimeStamp, true); }
			set	{ SetValue((int)GameFieldIndex.TimeStamp, value); }
		}

		/// <summary> The WinnerId property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."WinnerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 WinnerId
		{
			get { return (System.Int32)GetValue((int)GameFieldIndex.WinnerId, true); }
			set	{ SetValue((int)GameFieldIndex.WinnerId, value); }
		}

		/// <summary> The WinnerOriginalRating property of the Entity Game<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblGame"."WinnerOriginalRating"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 WinnerOriginalRating
		{
			get { return (System.Int16)GetValue((int)GameFieldIndex.WinnerOriginalRating, true); }
			set	{ SetValue((int)GameFieldIndex.WinnerOriginalRating, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'PostGameRatingEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PostGameRatingEntity))]
		public virtual EntityCollection<PostGameRatingEntity> PostGameRatings
		{
			get { return GetOrCreateEntityCollection<PostGameRatingEntity, PostGameRatingEntityFactory>("Game", true, false, ref _postGameRatings);	}
		}

		/// <summary> Gets / sets related entity of type 'LeaguePlayerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LeaguePlayerEntity Loser
		{
			get	{ return _loser; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncLoser(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Games", "Loser", _loser, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'LeaguePlayerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LeaguePlayerEntity Winner
		{
			get	{ return _winner; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncWinner(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "Games1", "Winner", _winner, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the RatingTracker.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)RatingTracker.EntityType.GameEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
