﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.ComponentModel;
using System.Collections.Generic;
#if !CF
using System.Runtime.Serialization;
#endif
using System.Xml.Serialization;
using RatingTracker;
using RatingTracker.HelperClasses;
using RatingTracker.FactoryClasses;
using RatingTracker.RelationClasses;

using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.EntityClasses
{
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	/// <summary>Entity class which represents the entity 'LeaguePlayer'.<br/><br/></summary>
	[Serializable]
	public partial class LeaguePlayerEntity : CommonEntityBase
		// __LLBLGENPRO_USER_CODE_REGION_START AdditionalInterfaces
		// __LLBLGENPRO_USER_CODE_REGION_END	
	{
		#region Class Member Declarations
		private EntityCollection<GameEntity> _games;
		private EntityCollection<GameEntity> _games1;
		private EntityCollection<PostGameRatingEntity> _postGameRatings;
		private LeagueEntity _league;
		private PlayerEntity _player;

		// __LLBLGENPRO_USER_CODE_REGION_START PrivateMembers
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Statics
		private static Dictionary<string, string>	_customProperties;
		private static Dictionary<string, Dictionary<string, string>>	_fieldsCustomProperties;

		/// <summary>All names of fields mapped onto a relation. Usable for in-memory filtering</summary>
		public static partial class MemberNames
		{
			/// <summary>Member name League</summary>
			public static readonly string League = "League";
			/// <summary>Member name Player</summary>
			public static readonly string Player = "Player";
			/// <summary>Member name Games</summary>
			public static readonly string Games = "Games";
			/// <summary>Member name Games1</summary>
			public static readonly string Games1 = "Games1";
			/// <summary>Member name PostGameRatings</summary>
			public static readonly string PostGameRatings = "PostGameRatings";
		}
		#endregion
		
		/// <summary> Static CTor for setting up custom property hashtables. Is executed before the first instance of this entity class or derived classes is constructed. </summary>
		static LeaguePlayerEntity()
		{
			SetupCustomPropertyHashtables();
		}
		
		/// <summary> CTor</summary>
		public LeaguePlayerEntity():base("LeaguePlayerEntity")
		{
			InitClassEmpty(null, null);
		}

		/// <summary> CTor</summary>
		/// <remarks>For framework usage.</remarks>
		/// <param name="fields">Fields object to set as the fields for this entity.</param>
		public LeaguePlayerEntity(IEntityFields2 fields):base("LeaguePlayerEntity")
		{
			InitClassEmpty(null, fields);
		}

		/// <summary> CTor</summary>
		/// <param name="validator">The custom validator object for this LeaguePlayerEntity</param>
		public LeaguePlayerEntity(IValidator validator):base("LeaguePlayerEntity")
		{
			InitClassEmpty(validator, null);
		}
				
		/// <summary> CTor</summary>
		/// <param name="id">PK value for LeaguePlayer which data should be fetched into this LeaguePlayer object</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public LeaguePlayerEntity(System.Int32 id):base("LeaguePlayerEntity")
		{
			InitClassEmpty(null, null);
			this.Id = id;
		}

		/// <summary> CTor</summary>
		/// <param name="id">PK value for LeaguePlayer which data should be fetched into this LeaguePlayer object</param>
		/// <param name="validator">The custom validator object for this LeaguePlayerEntity</param>
		/// <remarks>The entity is not fetched by this constructor. Use a DataAccessAdapter for that.</remarks>
		public LeaguePlayerEntity(System.Int32 id, IValidator validator):base("LeaguePlayerEntity")
		{
			InitClassEmpty(validator, null);
			this.Id = id;
		}

		/// <summary> Protected CTor for deserialization</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected LeaguePlayerEntity(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if(SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				_games = (EntityCollection<GameEntity>)info.GetValue("_games", typeof(EntityCollection<GameEntity>));
				_games1 = (EntityCollection<GameEntity>)info.GetValue("_games1", typeof(EntityCollection<GameEntity>));
				_postGameRatings = (EntityCollection<PostGameRatingEntity>)info.GetValue("_postGameRatings", typeof(EntityCollection<PostGameRatingEntity>));
				_league = (LeagueEntity)info.GetValue("_league", typeof(LeagueEntity));
				if(_league!=null)
				{
					_league.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				_player = (PlayerEntity)info.GetValue("_player", typeof(PlayerEntity));
				if(_player!=null)
				{
					_player.AfterSave+=new EventHandler(OnEntityAfterSave);
				}
				this.FixupDeserialization(FieldInfoProviderSingleton.GetInstance());
			}
			// __LLBLGENPRO_USER_CODE_REGION_START DeserializationConstructor
			// __LLBLGENPRO_USER_CODE_REGION_END
		}

		
		/// <summary>Performs the desync setup when an FK field has been changed. The entity referenced based on the FK field will be dereferenced and sync info will be removed.</summary>
		/// <param name="fieldIndex">The fieldindex.</param>
		protected override void PerformDesyncSetupFKFieldChange(int fieldIndex)
		{
			switch((LeaguePlayerFieldIndex)fieldIndex)
			{
				case LeaguePlayerFieldIndex.LeagueId:
					DesetupSyncLeague(true, false);
					break;
				case LeaguePlayerFieldIndex.PlayerId:
					DesetupSyncPlayer(true, false);
					break;
				default:
					base.PerformDesyncSetupFKFieldChange(fieldIndex);
					break;
			}
		}

		/// <summary> Sets the related entity property to the entity specified. If the property is a collection, it will add the entity specified to that collection.</summary>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="entity">Entity to set as an related entity</param>
		/// <remarks>Used by prefetch path logic.</remarks>
		protected override void SetRelatedEntityProperty(string propertyName, IEntityCore entity)
		{
			switch(propertyName)
			{
				case "League":
					this.League = (LeagueEntity)entity;
					break;
				case "Player":
					this.Player = (PlayerEntity)entity;
					break;
				case "Games":
					this.Games.Add((GameEntity)entity);
					break;
				case "Games1":
					this.Games1.Add((GameEntity)entity);
					break;
				case "PostGameRatings":
					this.PostGameRatings.Add((PostGameRatingEntity)entity);
					break;
				default:
					this.OnSetRelatedEntityProperty(propertyName, entity);
					break;
			}
		}
		
		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		protected override RelationCollection GetRelationsForFieldOfType(string fieldName)
		{
			return GetRelationsForField(fieldName);
		}

		/// <summary>Gets the relation objects which represent the relation the fieldName specified is mapped on. </summary>
		/// <param name="fieldName">Name of the field mapped onto the relation of which the relation objects have to be obtained.</param>
		/// <returns>RelationCollection with relation object(s) which represent the relation the field is maped on</returns>
		internal static RelationCollection GetRelationsForField(string fieldName)
		{
			RelationCollection toReturn = new RelationCollection();
			switch(fieldName)
			{
				case "League":
					toReturn.Add(Relations.LeagueEntityUsingLeagueId);
					break;
				case "Player":
					toReturn.Add(Relations.PlayerEntityUsingPlayerId);
					break;
				case "Games":
					toReturn.Add(Relations.GameEntityUsingLoserId);
					break;
				case "Games1":
					toReturn.Add(Relations.GameEntityUsingWinnerId);
					break;
				case "PostGameRatings":
					toReturn.Add(Relations.PostGameRatingEntityUsingLeaguePlayerId);
					break;
				default:
					break;				
			}
			return toReturn;
		}
#if !CF
		/// <summary>Checks if the relation mapped by the property with the name specified is a one way / single sided relation. If the passed in name is null, it/ will return true if the entity has any single-sided relation</summary>
		/// <param name="propertyName">Name of the property which is mapped onto the relation to check, or null to check if the entity has any relation/ which is single sided</param>
		/// <returns>true if the relation is single sided / one way (so the opposite relation isn't present), false otherwise</returns>
		protected override bool CheckOneWayRelations(string propertyName)
		{
			int numberOfOneWayRelations = 0;
			switch(propertyName)
			{
				case null:
					return ((numberOfOneWayRelations > 0) || base.CheckOneWayRelations(null));
				default:
					return base.CheckOneWayRelations(propertyName);
			}
		}
#endif
		/// <summary> Sets the internal parameter related to the fieldname passed to the instance relatedEntity. </summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		protected override void SetRelatedEntity(IEntityCore relatedEntity, string fieldName)
		{
			switch(fieldName)
			{
				case "League":
					SetupSyncLeague(relatedEntity);
					break;
				case "Player":
					SetupSyncPlayer(relatedEntity);
					break;
				case "Games":
					this.Games.Add((GameEntity)relatedEntity);
					break;
				case "Games1":
					this.Games1.Add((GameEntity)relatedEntity);
					break;
				case "PostGameRatings":
					this.PostGameRatings.Add((PostGameRatingEntity)relatedEntity);
					break;
				default:
					break;
			}
		}

		/// <summary> Unsets the internal parameter related to the fieldname passed to the instance relatedEntity. Reverses the actions taken by SetRelatedEntity() </summary>
		/// <param name="relatedEntity">Instance to unset as the related entity of type entityType</param>
		/// <param name="fieldName">Name of field mapped onto the relation which resolves in the instance relatedEntity</param>
		/// <param name="signalRelatedEntityManyToOne">if set to true it will notify the manytoone side, if applicable.</param>
		protected override void UnsetRelatedEntity(IEntityCore relatedEntity, string fieldName, bool signalRelatedEntityManyToOne)
		{
			switch(fieldName)
			{
				case "League":
					DesetupSyncLeague(false, true);
					break;
				case "Player":
					DesetupSyncPlayer(false, true);
					break;
				case "Games":
					this.PerformRelatedEntityRemoval(this.Games, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "Games1":
					this.PerformRelatedEntityRemoval(this.Games1, relatedEntity, signalRelatedEntityManyToOne);
					break;
				case "PostGameRatings":
					this.PerformRelatedEntityRemoval(this.PostGameRatings, relatedEntity, signalRelatedEntityManyToOne);
					break;
				default:
					break;
			}
		}

		/// <summary> Gets a collection of related entities referenced by this entity which depend on this entity (this entity is the PK side of their FK fields). These entities will have to be persisted after this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependingRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			return toReturn;
		}
		
		/// <summary> Gets a collection of related entities referenced by this entity which this entity depends on (this entity is the FK side of their PK fields). These
		/// entities will have to be persisted before this entity during a recursive save.</summary>
		/// <returns>Collection with 0 or more IEntity2 objects, referenced by this entity</returns>
		protected override List<IEntity2> GetDependentRelatedEntities()
		{
			List<IEntity2> toReturn = new List<IEntity2>();
			if(_league!=null)
			{
				toReturn.Add(_league);
			}
			if(_player!=null)
			{
				toReturn.Add(_player);
			}
			return toReturn;
		}
		
		/// <summary>Gets a list of all entity collections stored as member variables in this entity. Only 1:n related collections are returned.</summary>
		/// <returns>Collection with 0 or more IEntityCollection2 objects, referenced by this entity</returns>
		protected override List<IEntityCollection2> GetMemberEntityCollections()
		{
			List<IEntityCollection2> toReturn = new List<IEntityCollection2>();
			toReturn.Add(this.Games);
			toReturn.Add(this.Games1);
			toReturn.Add(this.PostGameRatings);
			return toReturn;
		}

		/// <summary>ISerializable member. Does custom serialization so event handlers do not get serialized. Serializes members of this entity class and uses the base class' implementation to serialize the rest.</summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (SerializationHelper.Optimization != SerializationOptimization.Fast) 
			{
				info.AddValue("_games", ((_games!=null) && (_games.Count>0) && !this.MarkedForDeletion)?_games:null);
				info.AddValue("_games1", ((_games1!=null) && (_games1.Count>0) && !this.MarkedForDeletion)?_games1:null);
				info.AddValue("_postGameRatings", ((_postGameRatings!=null) && (_postGameRatings.Count>0) && !this.MarkedForDeletion)?_postGameRatings:null);
				info.AddValue("_league", (!this.MarkedForDeletion?_league:null));
				info.AddValue("_player", (!this.MarkedForDeletion?_player:null));
			}
			// __LLBLGENPRO_USER_CODE_REGION_START GetObjectInfo
			// __LLBLGENPRO_USER_CODE_REGION_END
			base.GetObjectData(info, context);
		}

		/// <summary> Method which will construct a filter (predicate expression) for the unique constraint defined on the fields:
		/// LeagueId , PlayerId .</summary>
		/// <returns>true if succeeded and the contents is read, false otherwise</returns>
		public IPredicateExpression ConstructFilterForUCLeagueIdPlayerId()
		{
			IPredicateExpression filter = new PredicateExpression();
			filter.Add(RatingTracker.HelperClasses.LeaguePlayerFields.LeagueId == this.Fields.GetCurrentValue((int)LeaguePlayerFieldIndex.LeagueId));
			filter.Add(RatingTracker.HelperClasses.LeaguePlayerFields.PlayerId == this.Fields.GetCurrentValue((int)LeaguePlayerFieldIndex.PlayerId));
 			return filter;
		}


				
		/// <summary>Gets a list of all the EntityRelation objects the type of this instance has.</summary>
		/// <returns>A list of all the EntityRelation objects the type of this instance has. Hierarchy relations are excluded.</returns>
		protected override List<IEntityRelation> GetAllRelations()
		{
			return new LeaguePlayerRelations().GetAllRelations();
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Game' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoGames()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(GameFields.LoserId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'Game' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoGames1()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(GameFields.WinnerId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entities of type 'PostGameRating' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPostGameRatings()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(PostGameRatingFields.LeaguePlayerId, null, ComparisonOperator.Equal, this.Id));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'League' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoLeague()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(LeagueFields.Id, null, ComparisonOperator.Equal, this.LeagueId));
			return bucket;
		}

		/// <summary> Creates a new IRelationPredicateBucket object which contains the predicate expression and relation collection to fetch the related entity of type 'Player' to this entity.</summary>
		/// <returns></returns>
		public virtual IRelationPredicateBucket GetRelationInfoPlayer()
		{
			IRelationPredicateBucket bucket = new RelationPredicateBucket();
			bucket.PredicateExpression.Add(new FieldCompareValuePredicate(PlayerFields.Id, null, ComparisonOperator.Equal, this.PlayerId));
			return bucket;
		}
		

		/// <summary>Creates a new instance of the factory related to this entity</summary>
		protected override IEntityFactory2 CreateEntityFactory()
		{
			return EntityFactoryCache2.GetEntityFactory(typeof(LeaguePlayerEntityFactory));
		}
#if !CF
		/// <summary>Adds the member collections to the collections queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void AddToMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue) 
		{
			base.AddToMemberEntityCollectionsQueue(collectionsQueue);
			collectionsQueue.Enqueue(this._games);
			collectionsQueue.Enqueue(this._games1);
			collectionsQueue.Enqueue(this._postGameRatings);
		}
		
		/// <summary>Gets the member collections queue from the queue (base first)</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		protected override void GetFromMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue)
		{
			base.GetFromMemberEntityCollectionsQueue(collectionsQueue);
			this._games = (EntityCollection<GameEntity>) collectionsQueue.Dequeue();
			this._games1 = (EntityCollection<GameEntity>) collectionsQueue.Dequeue();
			this._postGameRatings = (EntityCollection<PostGameRatingEntity>) collectionsQueue.Dequeue();

		}
		
		/// <summary>Determines whether the entity has populated member collections</summary>
		/// <returns>true if the entity has populated member collections.</returns>
		protected override bool HasPopulatedMemberEntityCollections()
		{
			bool toReturn = false;
			toReturn |=(this._games != null);
			toReturn |=(this._games1 != null);
			toReturn |=(this._postGameRatings != null);
			return toReturn ? true : base.HasPopulatedMemberEntityCollections();
		}
		
		/// <summary>Creates the member entity collections queue.</summary>
		/// <param name="collectionsQueue">The collections queue.</param>
		/// <param name="requiredQueue">The required queue.</param>
		protected override void CreateMemberEntityCollectionsQueue(Queue<IEntityCollection2> collectionsQueue, Queue<bool> requiredQueue) 
		{
			base.CreateMemberEntityCollectionsQueue(collectionsQueue, requiredQueue);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<GameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<GameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory))) : null);
			collectionsQueue.Enqueue(requiredQueue.Dequeue() ? new EntityCollection<PostGameRatingEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PostGameRatingEntityFactory))) : null);
		}
#endif
		/// <summary>Gets all related data objects, stored by name. The name is the field name mapped onto the relation for that particular data element.</summary>
		/// <returns>Dictionary with per name the related referenced data element, which can be an entity collection or an entity or null</returns>
		protected override Dictionary<string, object> GetRelatedData()
		{
			Dictionary<string, object> toReturn = new Dictionary<string, object>();
			toReturn.Add("League", _league);
			toReturn.Add("Player", _player);
			toReturn.Add("Games", _games);
			toReturn.Add("Games1", _games1);
			toReturn.Add("PostGameRatings", _postGameRatings);
			return toReturn;
		}

		/// <summary> Initializes the class members</summary>
		private void InitClassMembers()
		{
			PerformDependencyInjection();
			
			// __LLBLGENPRO_USER_CODE_REGION_START InitClassMembers
			// __LLBLGENPRO_USER_CODE_REGION_END
			OnInitClassMembersComplete();
		}


		#region Custom Property Hashtable Setup
		/// <summary> Initializes the hashtables for the entity type and entity field custom properties. </summary>
		private static void SetupCustomPropertyHashtables()
		{
			_customProperties = new Dictionary<string, string>();
			_fieldsCustomProperties = new Dictionary<string, Dictionary<string, string>>();
			Dictionary<string, string> fieldHashtable;
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("CurrentRating", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("Id", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("LeagueId", fieldHashtable);
			fieldHashtable = new Dictionary<string, string>();
			_fieldsCustomProperties.Add("PlayerId", fieldHashtable);
		}
		#endregion

		/// <summary> Removes the sync logic for member _league</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncLeague(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _league, new PropertyChangedEventHandler( OnLeaguePropertyChanged ), "League", RatingTracker.RelationClasses.StaticLeaguePlayerRelations.LeagueEntityUsingLeagueIdStatic, true, signalRelatedEntity, "LeaguePlayers", resetFKFields, new int[] { (int)LeaguePlayerFieldIndex.LeagueId } );
			_league = null;
		}

		/// <summary> setups the sync logic for member _league</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncLeague(IEntityCore relatedEntity)
		{
			if(_league!=relatedEntity)
			{
				DesetupSyncLeague(true, true);
				_league = (LeagueEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _league, new PropertyChangedEventHandler( OnLeaguePropertyChanged ), "League", RatingTracker.RelationClasses.StaticLeaguePlayerRelations.LeagueEntityUsingLeagueIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnLeaguePropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Removes the sync logic for member _player</summary>
		/// <param name="signalRelatedEntity">If set to true, it will call the related entity's UnsetRelatedEntity method</param>
		/// <param name="resetFKFields">if set to true it will also reset the FK fields pointing to the related entity</param>
		private void DesetupSyncPlayer(bool signalRelatedEntity, bool resetFKFields)
		{
			this.PerformDesetupSyncRelatedEntity( _player, new PropertyChangedEventHandler( OnPlayerPropertyChanged ), "Player", RatingTracker.RelationClasses.StaticLeaguePlayerRelations.PlayerEntityUsingPlayerIdStatic, true, signalRelatedEntity, "LeaguePlayers", resetFKFields, new int[] { (int)LeaguePlayerFieldIndex.PlayerId } );
			_player = null;
		}

		/// <summary> setups the sync logic for member _player</summary>
		/// <param name="relatedEntity">Instance to set as the related entity of type entityType</param>
		private void SetupSyncPlayer(IEntityCore relatedEntity)
		{
			if(_player!=relatedEntity)
			{
				DesetupSyncPlayer(true, true);
				_player = (PlayerEntity)relatedEntity;
				this.PerformSetupSyncRelatedEntity( _player, new PropertyChangedEventHandler( OnPlayerPropertyChanged ), "Player", RatingTracker.RelationClasses.StaticLeaguePlayerRelations.PlayerEntityUsingPlayerIdStatic, true, new string[] {  } );
			}
		}
		
		/// <summary>Handles property change events of properties in a related entity.</summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void OnPlayerPropertyChanged( object sender, PropertyChangedEventArgs e )
		{
			switch( e.PropertyName )
			{
				default:
					break;
			}
		}

		/// <summary> Initializes the class with empty data, as if it is a new Entity.</summary>
		/// <param name="validator">The validator object for this LeaguePlayerEntity</param>
		/// <param name="fields">Fields of this entity</param>
		private void InitClassEmpty(IValidator validator, IEntityFields2 fields)
		{
			OnInitializing();
			this.Fields = fields ?? CreateFields();
			this.Validator = validator;
			InitClassMembers();

			// __LLBLGENPRO_USER_CODE_REGION_START InitClassEmpty
			// __LLBLGENPRO_USER_CODE_REGION_END

			OnInitialized();

		}

		#region Class Property Declarations
		/// <summary> The relations object holding all relations of this entity with other entity classes.</summary>
		public  static LeaguePlayerRelations Relations
		{
			get	{ return new LeaguePlayerRelations(); }
		}
		
		/// <summary> The custom properties for this entity type.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, string> CustomProperties
		{
			get { return _customProperties;}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Game' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathGames
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<GameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory))), (IEntityRelation)GetRelationsForField("Games")[0], (int)RatingTracker.EntityType.LeaguePlayerEntity, (int)RatingTracker.EntityType.GameEntity, 0, null, null, null, null, "Games", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Game' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathGames1
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<GameEntity>(EntityFactoryCache2.GetEntityFactory(typeof(GameEntityFactory))), (IEntityRelation)GetRelationsForField("Games1")[0], (int)RatingTracker.EntityType.LeaguePlayerEntity, (int)RatingTracker.EntityType.GameEntity, 0, null, null, null, null, "Games1", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'PostGameRating' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPostGameRatings
		{
			get	{ return new PrefetchPathElement2( new EntityCollection<PostGameRatingEntity>(EntityFactoryCache2.GetEntityFactory(typeof(PostGameRatingEntityFactory))), (IEntityRelation)GetRelationsForField("PostGameRatings")[0], (int)RatingTracker.EntityType.LeaguePlayerEntity, (int)RatingTracker.EntityType.PostGameRatingEntity, 0, null, null, null, null, "PostGameRatings", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany);	}
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'League' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathLeague
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(LeagueEntityFactory))),	(IEntityRelation)GetRelationsForField("League")[0], (int)RatingTracker.EntityType.LeaguePlayerEntity, (int)RatingTracker.EntityType.LeagueEntity, 0, null, null, null, null, "League", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}

		/// <summary> Creates a new PrefetchPathElement2 object which contains all the information to prefetch the related entities of type 'Player' for this entity.</summary>
		/// <returns>Ready to use IPrefetchPathElement2 implementation.</returns>
		public static IPrefetchPathElement2 PrefetchPathPlayer
		{
			get	{ return new PrefetchPathElement2(new EntityCollection(EntityFactoryCache2.GetEntityFactory(typeof(PlayerEntityFactory))),	(IEntityRelation)GetRelationsForField("Player")[0], (int)RatingTracker.EntityType.LeaguePlayerEntity, (int)RatingTracker.EntityType.PlayerEntity, 0, null, null, null, null, "Player", SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne); }
		}


		/// <summary> The custom properties for the type of this entity instance.</summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, string> CustomPropertiesOfType
		{
			get { return CustomProperties;}
		}

		/// <summary> The custom properties for the fields of this entity type. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		public  static Dictionary<string, Dictionary<string, string>> FieldsCustomProperties
		{
			get { return _fieldsCustomProperties;}
		}

		/// <summary> The custom properties for the fields of the type of this entity instance. The returned Hashtable contains per fieldname a hashtable of name-value pairs. </summary>
		/// <remarks>The data returned from this property should be considered read-only: it is not thread safe to alter this data at runtime.</remarks>
		[Browsable(false), XmlIgnore]
		protected override Dictionary<string, Dictionary<string, string>> FieldsCustomPropertiesOfType
		{
			get { return FieldsCustomProperties;}
		}

		/// <summary> The CurrentRating property of the Entity LeaguePlayer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLeaguePlayer"."CurrentRating"<br/>
		/// Table field type characteristics (type, precision, scale, length): SmallInt, 5, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int16 CurrentRating
		{
			get { return (System.Int16)GetValue((int)LeaguePlayerFieldIndex.CurrentRating, true); }
			set	{ SetValue((int)LeaguePlayerFieldIndex.CurrentRating, value); }
		}

		/// <summary> The Id property of the Entity LeaguePlayer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLeaguePlayer"."Id"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, true, true</remarks>
		public virtual System.Int32 Id
		{
			get { return (System.Int32)GetValue((int)LeaguePlayerFieldIndex.Id, true); }
			set	{ SetValue((int)LeaguePlayerFieldIndex.Id, value); }
		}

		/// <summary> The LeagueId property of the Entity LeaguePlayer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLeaguePlayer"."LeagueId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 LeagueId
		{
			get { return (System.Int32)GetValue((int)LeaguePlayerFieldIndex.LeagueId, true); }
			set	{ SetValue((int)LeaguePlayerFieldIndex.LeagueId, value); }
		}

		/// <summary> The PlayerId property of the Entity LeaguePlayer<br/><br/></summary>
		/// <remarks>Mapped on  table field: "tblLeaguePlayer"."PlayerId"<br/>
		/// Table field type characteristics (type, precision, scale, length): Int, 10, 0, 0<br/>
		/// Table field behavior characteristics (is nullable, is PK, is identity): false, false, false</remarks>
		public virtual System.Int32 PlayerId
		{
			get { return (System.Int32)GetValue((int)LeaguePlayerFieldIndex.PlayerId, true); }
			set	{ SetValue((int)LeaguePlayerFieldIndex.PlayerId, value); }
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'GameEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(GameEntity))]
		public virtual EntityCollection<GameEntity> Games
		{
			get { return GetOrCreateEntityCollection<GameEntity, GameEntityFactory>("Loser", true, false, ref _games);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'GameEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(GameEntity))]
		public virtual EntityCollection<GameEntity> Games1
		{
			get { return GetOrCreateEntityCollection<GameEntity, GameEntityFactory>("Winner", true, false, ref _games1);	}
		}

		/// <summary> Gets the EntityCollection with the related entities of type 'PostGameRatingEntity' which are related to this entity via a relation of type '1:n'. If the EntityCollection hasn't been fetched yet, the collection returned will be empty.<br/><br/></summary>
		[TypeContainedAttribute(typeof(PostGameRatingEntity))]
		public virtual EntityCollection<PostGameRatingEntity> PostGameRatings
		{
			get { return GetOrCreateEntityCollection<PostGameRatingEntity, PostGameRatingEntityFactory>("LeaguePlayer", true, false, ref _postGameRatings);	}
		}

		/// <summary> Gets / sets related entity of type 'LeagueEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual LeagueEntity League
		{
			get	{ return _league; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncLeague(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LeaguePlayers", "League", _league, true); 
				}
			}
		}

		/// <summary> Gets / sets related entity of type 'PlayerEntity' which has to be set using a fetch action earlier. If no related entity is set for this property, null is returned..<br/><br/></summary>
		[Browsable(false)]
		public virtual PlayerEntity Player
		{
			get	{ return _player; }
			set
			{
				if(this.IsDeserializing)
				{
					SetupSyncPlayer(value);
				}
				else
				{
					SetSingleRelatedEntityNavigator(value, "LeaguePlayers", "Player", _player, true); 
				}
			}
		}
	
		/// <summary> Gets the type of the hierarchy this entity is in. </summary>
		protected override InheritanceHierarchyType LLBLGenProIsInHierarchyOfType
		{
			get { return InheritanceHierarchyType.None;}
		}
		
		/// <summary> Gets or sets a value indicating whether this entity is a subtype</summary>
		protected override bool LLBLGenProIsSubType
		{
			get { return false;}
		}
		
		/// <summary>Returns the RatingTracker.EntityType enum value for this entity.</summary>
		[Browsable(false), XmlIgnore]
		protected override int LLBLGenProEntityTypeValue 
		{ 
			get { return (int)RatingTracker.EntityType.LeaguePlayerEntity; }
		}

		#endregion


		#region Custom Entity code
		
		// __LLBLGENPRO_USER_CODE_REGION_START CustomEntityCode
		// __LLBLGENPRO_USER_CODE_REGION_END
		#endregion

		#region Included code

		#endregion
	}
}
