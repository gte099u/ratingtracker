﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;
using RatingTracker.FactoryClasses;
using RatingTracker;

namespace RatingTracker.HelperClasses
{
	/// <summary>Field Creation Class for entity GameEntity</summary>
	public partial class GameFields
	{
		/// <summary>Creates a new GameEntity.Bias field instance</summary>
		public static EntityField2 Bias
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.Bias);}
		}
		/// <summary>Creates a new GameEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.Id);}
		}
		/// <summary>Creates a new GameEntity.LoserId field instance</summary>
		public static EntityField2 LoserId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.LoserId);}
		}
		/// <summary>Creates a new GameEntity.LoserOriginalRating field instance</summary>
		public static EntityField2 LoserOriginalRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.LoserOriginalRating);}
		}
		/// <summary>Creates a new GameEntity.TimeStamp field instance</summary>
		public static EntityField2 TimeStamp
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.TimeStamp);}
		}
		/// <summary>Creates a new GameEntity.WinnerId field instance</summary>
		public static EntityField2 WinnerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.WinnerId);}
		}
		/// <summary>Creates a new GameEntity.WinnerOriginalRating field instance</summary>
		public static EntityField2 WinnerOriginalRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(GameFieldIndex.WinnerOriginalRating);}
		}
	}

	/// <summary>Field Creation Class for entity LeagueEntity</summary>
	public partial class LeagueFields
	{
		/// <summary>Creates a new LeagueEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueFieldIndex.Id);}
		}
		/// <summary>Creates a new LeagueEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueFieldIndex.Name);}
		}
	}

	/// <summary>Field Creation Class for entity LeagueGameDetailEntity</summary>
	public partial class LeagueGameDetailFields
	{
		/// <summary>Creates a new LeagueGameDetailEntity.Bias field instance</summary>
		public static EntityField2 Bias
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.Bias);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.GameId field instance</summary>
		public static EntityField2 GameId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.GameId);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.LeagueId field instance</summary>
		public static EntityField2 LeagueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.LeagueId);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.LoserId field instance</summary>
		public static EntityField2 LoserId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.LoserId);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.LoserName field instance</summary>
		public static EntityField2 LoserName
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.LoserName);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.LoserOriginalRating field instance</summary>
		public static EntityField2 LoserOriginalRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.LoserOriginalRating);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.LoserPlayerId field instance</summary>
		public static EntityField2 LoserPlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.LoserPlayerId);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.TimeStamp field instance</summary>
		public static EntityField2 TimeStamp
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.TimeStamp);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.WinnerId field instance</summary>
		public static EntityField2 WinnerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.WinnerId);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.WinnerName field instance</summary>
		public static EntityField2 WinnerName
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.WinnerName);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.WinnerOriginalRating field instance</summary>
		public static EntityField2 WinnerOriginalRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.WinnerOriginalRating);}
		}
		/// <summary>Creates a new LeagueGameDetailEntity.WinnerPlayerId field instance</summary>
		public static EntityField2 WinnerPlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeagueGameDetailFieldIndex.WinnerPlayerId);}
		}
	}

	/// <summary>Field Creation Class for entity LeaguePlayerEntity</summary>
	public partial class LeaguePlayerFields
	{
		/// <summary>Creates a new LeaguePlayerEntity.CurrentRating field instance</summary>
		public static EntityField2 CurrentRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerFieldIndex.CurrentRating);}
		}
		/// <summary>Creates a new LeaguePlayerEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerFieldIndex.Id);}
		}
		/// <summary>Creates a new LeaguePlayerEntity.LeagueId field instance</summary>
		public static EntityField2 LeagueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerFieldIndex.LeagueId);}
		}
		/// <summary>Creates a new LeaguePlayerEntity.PlayerId field instance</summary>
		public static EntityField2 PlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerFieldIndex.PlayerId);}
		}
	}

	/// <summary>Field Creation Class for entity LeaguePlayerDetailEntity</summary>
	public partial class LeaguePlayerDetailFields
	{
		/// <summary>Creates a new LeaguePlayerDetailEntity.CurrentRating field instance</summary>
		public static EntityField2 CurrentRating
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.CurrentRating);}
		}
		/// <summary>Creates a new LeaguePlayerDetailEntity.LeagueId field instance</summary>
		public static EntityField2 LeagueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.LeagueId);}
		}
		/// <summary>Creates a new LeaguePlayerDetailEntity.LeagueName field instance</summary>
		public static EntityField2 LeagueName
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.LeagueName);}
		}
		/// <summary>Creates a new LeaguePlayerDetailEntity.LeaguePlayerId field instance</summary>
		public static EntityField2 LeaguePlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.LeaguePlayerId);}
		}
		/// <summary>Creates a new LeaguePlayerDetailEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.Name);}
		}
		/// <summary>Creates a new LeaguePlayerDetailEntity.PlayerId field instance</summary>
		public static EntityField2 PlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(LeaguePlayerDetailFieldIndex.PlayerId);}
		}
	}

	/// <summary>Field Creation Class for entity PlayerEntity</summary>
	public partial class PlayerFields
	{
		/// <summary>Creates a new PlayerEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.Id);}
		}
		/// <summary>Creates a new PlayerEntity.Name field instance</summary>
		public static EntityField2 Name
		{
			get { return (EntityField2)EntityFieldFactory.Create(PlayerFieldIndex.Name);}
		}
	}

	/// <summary>Field Creation Class for entity PostGameRatingEntity</summary>
	public partial class PostGameRatingFields
	{
		/// <summary>Creates a new PostGameRatingEntity.GameId field instance</summary>
		public static EntityField2 GameId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingFieldIndex.GameId);}
		}
		/// <summary>Creates a new PostGameRatingEntity.Id field instance</summary>
		public static EntityField2 Id
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingFieldIndex.Id);}
		}
		/// <summary>Creates a new PostGameRatingEntity.LeaguePlayerId field instance</summary>
		public static EntityField2 LeaguePlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingFieldIndex.LeaguePlayerId);}
		}
		/// <summary>Creates a new PostGameRatingEntity.Rating field instance</summary>
		public static EntityField2 Rating
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingFieldIndex.Rating);}
		}
	}

	/// <summary>Field Creation Class for entity PostGameRatingDetailEntity</summary>
	public partial class PostGameRatingDetailFields
	{
		/// <summary>Creates a new PostGameRatingDetailEntity.GameId field instance</summary>
		public static EntityField2 GameId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.GameId);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.LeagueId field instance</summary>
		public static EntityField2 LeagueId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.LeagueId);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.LeagueName field instance</summary>
		public static EntityField2 LeagueName
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.LeagueName);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.LeaguePlayerId field instance</summary>
		public static EntityField2 LeaguePlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.LeaguePlayerId);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.PlayerId field instance</summary>
		public static EntityField2 PlayerId
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.PlayerId);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.PlayerName field instance</summary>
		public static EntityField2 PlayerName
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.PlayerName);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.Rating field instance</summary>
		public static EntityField2 Rating
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.Rating);}
		}
		/// <summary>Creates a new PostGameRatingDetailEntity.TimeStamp field instance</summary>
		public static EntityField2 TimeStamp
		{
			get { return (EntityField2)EntityFieldFactory.Create(PostGameRatingDetailFieldIndex.TimeStamp);}
		}
	}
	

}