﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.HelperClasses
{
	
	// __LLBLGENPRO_USER_CODE_REGION_START AdditionalNamespaces
	// __LLBLGENPRO_USER_CODE_REGION_END
	
	/// <summary>Singleton implementation of the FieldInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the FieldInfoProviderBase class is threadsafe.</remarks>
	internal static class FieldInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IFieldInfoProvider _providerInstance = new FieldInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static FieldInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the FieldInfoProviderCore</summary>
		/// <returns>Instance of the FieldInfoProvider.</returns>
		public static IFieldInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the FieldInfoProvider. Used by singleton wrapper.</summary>
	internal class FieldInfoProviderCore : FieldInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="FieldInfoProviderCore"/> class.</summary>
		internal FieldInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores.</summary>
		private void Init()
		{
			this.InitClass( (8 + 0));
			InitGameEntityInfos();
			InitLeagueEntityInfos();
			InitLeagueGameDetailEntityInfos();
			InitLeaguePlayerEntityInfos();
			InitLeaguePlayerDetailEntityInfos();
			InitPlayerEntityInfos();
			InitPostGameRatingEntityInfos();
			InitPostGameRatingDetailEntityInfos();

			this.ConstructElementFieldStructures(InheritanceInfoProviderSingleton.GetInstance());
		}

		/// <summary>Inits GameEntity's FieldInfo objects</summary>
		private void InitGameEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(GameFieldIndex), "GameEntity");
			this.AddElementFieldInfo("GameEntity", "Bias", typeof(System.Double), false, false, false, false,  (int)GameFieldIndex.Bias, 0, 0, 38);
			this.AddElementFieldInfo("GameEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)GameFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("GameEntity", "LoserId", typeof(System.Int32), false, true, false, false,  (int)GameFieldIndex.LoserId, 0, 0, 10);
			this.AddElementFieldInfo("GameEntity", "LoserOriginalRating", typeof(System.Int16), false, false, false, false,  (int)GameFieldIndex.LoserOriginalRating, 0, 0, 5);
			this.AddElementFieldInfo("GameEntity", "TimeStamp", typeof(System.DateTime), false, false, false, false,  (int)GameFieldIndex.TimeStamp, 0, 0, 0);
			this.AddElementFieldInfo("GameEntity", "WinnerId", typeof(System.Int32), false, true, false, false,  (int)GameFieldIndex.WinnerId, 0, 0, 10);
			this.AddElementFieldInfo("GameEntity", "WinnerOriginalRating", typeof(System.Int16), false, false, false, false,  (int)GameFieldIndex.WinnerOriginalRating, 0, 0, 5);
		}
		/// <summary>Inits LeagueEntity's FieldInfo objects</summary>
		private void InitLeagueEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LeagueFieldIndex), "LeagueEntity");
			this.AddElementFieldInfo("LeagueEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)LeagueFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("LeagueEntity", "Name", typeof(System.String), false, false, false, false,  (int)LeagueFieldIndex.Name, 150, 0, 0);
		}
		/// <summary>Inits LeagueGameDetailEntity's FieldInfo objects</summary>
		private void InitLeagueGameDetailEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LeagueGameDetailFieldIndex), "LeagueGameDetailEntity");
			this.AddElementFieldInfo("LeagueGameDetailEntity", "Bias", typeof(System.Double), false, false, false, false,  (int)LeagueGameDetailFieldIndex.Bias, 0, 0, 38);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "GameId", typeof(System.Int32), true, false, false, false,  (int)LeagueGameDetailFieldIndex.GameId, 0, 0, 10);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "LeagueId", typeof(System.Int32), false, false, false, false,  (int)LeagueGameDetailFieldIndex.LeagueId, 0, 0, 10);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "LoserId", typeof(System.Int32), false, false, false, false,  (int)LeagueGameDetailFieldIndex.LoserId, 0, 0, 10);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "LoserName", typeof(System.String), false, false, false, false,  (int)LeagueGameDetailFieldIndex.LoserName, 150, 0, 0);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "LoserOriginalRating", typeof(System.Int16), false, false, false, false,  (int)LeagueGameDetailFieldIndex.LoserOriginalRating, 0, 0, 5);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "LoserPlayerId", typeof(System.Int32), false, false, false, false,  (int)LeagueGameDetailFieldIndex.LoserPlayerId, 0, 0, 10);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "TimeStamp", typeof(System.DateTime), false, false, false, false,  (int)LeagueGameDetailFieldIndex.TimeStamp, 0, 0, 0);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "WinnerId", typeof(System.Int32), false, false, false, false,  (int)LeagueGameDetailFieldIndex.WinnerId, 0, 0, 10);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "WinnerName", typeof(System.String), false, false, false, false,  (int)LeagueGameDetailFieldIndex.WinnerName, 150, 0, 0);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "WinnerOriginalRating", typeof(System.Int16), false, false, false, false,  (int)LeagueGameDetailFieldIndex.WinnerOriginalRating, 0, 0, 5);
			this.AddElementFieldInfo("LeagueGameDetailEntity", "WinnerPlayerId", typeof(System.Int32), false, false, false, false,  (int)LeagueGameDetailFieldIndex.WinnerPlayerId, 0, 0, 10);
		}
		/// <summary>Inits LeaguePlayerEntity's FieldInfo objects</summary>
		private void InitLeaguePlayerEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LeaguePlayerFieldIndex), "LeaguePlayerEntity");
			this.AddElementFieldInfo("LeaguePlayerEntity", "CurrentRating", typeof(System.Int16), false, false, false, false,  (int)LeaguePlayerFieldIndex.CurrentRating, 0, 0, 5);
			this.AddElementFieldInfo("LeaguePlayerEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)LeaguePlayerFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("LeaguePlayerEntity", "LeagueId", typeof(System.Int32), false, true, false, false,  (int)LeaguePlayerFieldIndex.LeagueId, 0, 0, 10);
			this.AddElementFieldInfo("LeaguePlayerEntity", "PlayerId", typeof(System.Int32), false, true, false, false,  (int)LeaguePlayerFieldIndex.PlayerId, 0, 0, 10);
		}
		/// <summary>Inits LeaguePlayerDetailEntity's FieldInfo objects</summary>
		private void InitLeaguePlayerDetailEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(LeaguePlayerDetailFieldIndex), "LeaguePlayerDetailEntity");
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "CurrentRating", typeof(System.Int16), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.CurrentRating, 0, 0, 5);
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "LeagueId", typeof(System.Int32), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.LeagueId, 0, 0, 10);
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "LeagueName", typeof(System.String), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.LeagueName, 150, 0, 0);
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "LeaguePlayerId", typeof(System.Int32), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.LeaguePlayerId, 0, 0, 10);
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "Name", typeof(System.String), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.Name, 150, 0, 0);
			this.AddElementFieldInfo("LeaguePlayerDetailEntity", "PlayerId", typeof(System.Int32), false, false, false, false,  (int)LeaguePlayerDetailFieldIndex.PlayerId, 0, 0, 10);
		}
		/// <summary>Inits PlayerEntity's FieldInfo objects</summary>
		private void InitPlayerEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PlayerFieldIndex), "PlayerEntity");
			this.AddElementFieldInfo("PlayerEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)PlayerFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("PlayerEntity", "Name", typeof(System.String), false, false, false, false,  (int)PlayerFieldIndex.Name, 150, 0, 0);
		}
		/// <summary>Inits PostGameRatingEntity's FieldInfo objects</summary>
		private void InitPostGameRatingEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PostGameRatingFieldIndex), "PostGameRatingEntity");
			this.AddElementFieldInfo("PostGameRatingEntity", "GameId", typeof(System.Int32), false, true, false, false,  (int)PostGameRatingFieldIndex.GameId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingEntity", "Id", typeof(System.Int32), true, false, true, false,  (int)PostGameRatingFieldIndex.Id, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingEntity", "LeaguePlayerId", typeof(System.Int32), false, true, false, false,  (int)PostGameRatingFieldIndex.LeaguePlayerId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingEntity", "Rating", typeof(System.Int16), false, false, false, false,  (int)PostGameRatingFieldIndex.Rating, 0, 0, 5);
		}
		/// <summary>Inits PostGameRatingDetailEntity's FieldInfo objects</summary>
		private void InitPostGameRatingDetailEntityInfos()
		{
			this.AddFieldIndexEnumForElementName(typeof(PostGameRatingDetailFieldIndex), "PostGameRatingDetailEntity");
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "GameId", typeof(System.Int32), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.GameId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "LeagueId", typeof(System.Int32), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.LeagueId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "LeagueName", typeof(System.String), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.LeagueName, 150, 0, 0);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "LeaguePlayerId", typeof(System.Int32), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.LeaguePlayerId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "PlayerId", typeof(System.Int32), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.PlayerId, 0, 0, 10);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "PlayerName", typeof(System.String), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.PlayerName, 150, 0, 0);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "Rating", typeof(System.Int16), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.Rating, 0, 0, 5);
			this.AddElementFieldInfo("PostGameRatingDetailEntity", "TimeStamp", typeof(System.DateTime), false, false, false, false,  (int)PostGameRatingDetailFieldIndex.TimeStamp, 0, 0, 0);
		}
		
	}
}




