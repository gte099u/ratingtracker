﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using RatingTracker;
using RatingTracker.FactoryClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: Game. </summary>
	public partial class GameRelations
	{
		/// <summary>CTor</summary>
		public GameRelations()
		{
		}

		/// <summary>Gets all relations of the GameEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.PostGameRatingEntityUsingGameId);
			toReturn.Add(this.LeaguePlayerEntityUsingLoserId);
			toReturn.Add(this.LeaguePlayerEntityUsingWinnerId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between GameEntity and PostGameRatingEntity over the 1:n relation they have, using the relation between the fields:
		/// Game.Id - PostGameRating.GameId
		/// </summary>
		public virtual IEntityRelation PostGameRatingEntityUsingGameId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PostGameRatings" , true);
				relation.AddEntityFieldPair(GameFields.Id, PostGameRatingFields.GameId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostGameRatingEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between GameEntity and LeaguePlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// Game.LoserId - LeaguePlayer.Id
		/// </summary>
		public virtual IEntityRelation LeaguePlayerEntityUsingLoserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Loser", false);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, GameFields.LoserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between GameEntity and LeaguePlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// Game.WinnerId - LeaguePlayer.Id
		/// </summary>
		public virtual IEntityRelation LeaguePlayerEntityUsingWinnerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Winner", false);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, GameFields.WinnerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticGameRelations
	{
		internal static readonly IEntityRelation PostGameRatingEntityUsingGameIdStatic = new GameRelations().PostGameRatingEntityUsingGameId;
		internal static readonly IEntityRelation LeaguePlayerEntityUsingLoserIdStatic = new GameRelations().LeaguePlayerEntityUsingLoserId;
		internal static readonly IEntityRelation LeaguePlayerEntityUsingWinnerIdStatic = new GameRelations().LeaguePlayerEntityUsingWinnerId;

		/// <summary>CTor</summary>
		static StaticGameRelations()
		{
		}
	}
}
