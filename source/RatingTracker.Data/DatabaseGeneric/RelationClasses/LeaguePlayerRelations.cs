﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using RatingTracker;
using RatingTracker.FactoryClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: LeaguePlayer. </summary>
	public partial class LeaguePlayerRelations
	{
		/// <summary>CTor</summary>
		public LeaguePlayerRelations()
		{
		}

		/// <summary>Gets all relations of the LeaguePlayerEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GameEntityUsingLoserId);
			toReturn.Add(this.GameEntityUsingWinnerId);
			toReturn.Add(this.PostGameRatingEntityUsingLeaguePlayerId);
			toReturn.Add(this.LeagueEntityUsingLeagueId);
			toReturn.Add(this.PlayerEntityUsingPlayerId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LeaguePlayerEntity and GameEntity over the 1:n relation they have, using the relation between the fields:
		/// LeaguePlayer.Id - Game.LoserId
		/// </summary>
		public virtual IEntityRelation GameEntityUsingLoserId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Games" , true);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, GameFields.LoserId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LeaguePlayerEntity and GameEntity over the 1:n relation they have, using the relation between the fields:
		/// LeaguePlayer.Id - Game.WinnerId
		/// </summary>
		public virtual IEntityRelation GameEntityUsingWinnerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "Games1" , true);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, GameFields.WinnerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				return relation;
			}
		}

		/// <summary>Returns a new IEntityRelation object, between LeaguePlayerEntity and PostGameRatingEntity over the 1:n relation they have, using the relation between the fields:
		/// LeaguePlayer.Id - PostGameRating.LeaguePlayerId
		/// </summary>
		public virtual IEntityRelation PostGameRatingEntityUsingLeaguePlayerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "PostGameRatings" , true);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, PostGameRatingFields.LeaguePlayerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostGameRatingEntity", false);
				return relation;
			}
		}


		/// <summary>Returns a new IEntityRelation object, between LeaguePlayerEntity and LeagueEntity over the m:1 relation they have, using the relation between the fields:
		/// LeaguePlayer.LeagueId - League.Id
		/// </summary>
		public virtual IEntityRelation LeagueEntityUsingLeagueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "League", false);
				relation.AddEntityFieldPair(LeagueFields.Id, LeaguePlayerFields.LeagueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeagueEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between LeaguePlayerEntity and PlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// LeaguePlayer.PlayerId - Player.Id
		/// </summary>
		public virtual IEntityRelation PlayerEntityUsingPlayerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Player", false);
				relation.AddEntityFieldPair(PlayerFields.Id, LeaguePlayerFields.PlayerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLeaguePlayerRelations
	{
		internal static readonly IEntityRelation GameEntityUsingLoserIdStatic = new LeaguePlayerRelations().GameEntityUsingLoserId;
		internal static readonly IEntityRelation GameEntityUsingWinnerIdStatic = new LeaguePlayerRelations().GameEntityUsingWinnerId;
		internal static readonly IEntityRelation PostGameRatingEntityUsingLeaguePlayerIdStatic = new LeaguePlayerRelations().PostGameRatingEntityUsingLeaguePlayerId;
		internal static readonly IEntityRelation LeagueEntityUsingLeagueIdStatic = new LeaguePlayerRelations().LeagueEntityUsingLeagueId;
		internal static readonly IEntityRelation PlayerEntityUsingPlayerIdStatic = new LeaguePlayerRelations().PlayerEntityUsingPlayerId;

		/// <summary>CTor</summary>
		static StaticLeaguePlayerRelations()
		{
		}
	}
}
