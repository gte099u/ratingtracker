﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using RatingTracker;
using RatingTracker.FactoryClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: League. </summary>
	public partial class LeagueRelations
	{
		/// <summary>CTor</summary>
		public LeagueRelations()
		{
		}

		/// <summary>Gets all relations of the LeagueEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.LeaguePlayerEntityUsingLeagueId);
			return toReturn;
		}

		#region Class Property Declarations

		/// <summary>Returns a new IEntityRelation object, between LeagueEntity and LeaguePlayerEntity over the 1:n relation they have, using the relation between the fields:
		/// League.Id - LeaguePlayer.LeagueId
		/// </summary>
		public virtual IEntityRelation LeaguePlayerEntityUsingLeagueId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.OneToMany, "LeaguePlayers" , true);
				relation.AddEntityFieldPair(LeagueFields.Id, LeaguePlayerFields.LeagueId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeagueEntity", true);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", false);
				return relation;
			}
		}


		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticLeagueRelations
	{
		internal static readonly IEntityRelation LeaguePlayerEntityUsingLeagueIdStatic = new LeagueRelations().LeaguePlayerEntityUsingLeagueId;

		/// <summary>CTor</summary>
		static StaticLeagueRelations()
		{
		}
	}
}
