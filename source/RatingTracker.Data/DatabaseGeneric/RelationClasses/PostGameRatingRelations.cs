﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
// Templates version: 
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Collections.Generic;
using RatingTracker;
using RatingTracker.FactoryClasses;
using RatingTracker.HelperClasses;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.RelationClasses
{
	/// <summary>Implements the relations factory for the entity: PostGameRating. </summary>
	public partial class PostGameRatingRelations
	{
		/// <summary>CTor</summary>
		public PostGameRatingRelations()
		{
		}

		/// <summary>Gets all relations of the PostGameRatingEntity as a list of IEntityRelation objects.</summary>
		/// <returns>a list of IEntityRelation objects</returns>
		public virtual List<IEntityRelation> GetAllRelations()
		{
			List<IEntityRelation> toReturn = new List<IEntityRelation>();
			toReturn.Add(this.GameEntityUsingGameId);
			toReturn.Add(this.LeaguePlayerEntityUsingLeaguePlayerId);
			return toReturn;
		}

		#region Class Property Declarations



		/// <summary>Returns a new IEntityRelation object, between PostGameRatingEntity and GameEntity over the m:1 relation they have, using the relation between the fields:
		/// PostGameRating.GameId - Game.Id
		/// </summary>
		public virtual IEntityRelation GameEntityUsingGameId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "Game", false);
				relation.AddEntityFieldPair(GameFields.Id, PostGameRatingFields.GameId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("GameEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostGameRatingEntity", true);
				return relation;
			}
		}
		/// <summary>Returns a new IEntityRelation object, between PostGameRatingEntity and LeaguePlayerEntity over the m:1 relation they have, using the relation between the fields:
		/// PostGameRating.LeaguePlayerId - LeaguePlayer.Id
		/// </summary>
		public virtual IEntityRelation LeaguePlayerEntityUsingLeaguePlayerId
		{
			get
			{
				IEntityRelation relation = new EntityRelation(SD.LLBLGen.Pro.ORMSupportClasses.RelationType.ManyToOne, "LeaguePlayer", false);
				relation.AddEntityFieldPair(LeaguePlayerFields.Id, PostGameRatingFields.LeaguePlayerId);
				relation.InheritanceInfoPkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("LeaguePlayerEntity", false);
				relation.InheritanceInfoFkSideEntity = InheritanceInfoProviderSingleton.GetInstance().GetInheritanceInfo("PostGameRatingEntity", true);
				return relation;
			}
		}
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSubTypeRelation(string subTypeEntityName) { return null; }
		/// <summary>stub, not used in this entity, only for TargetPerEntity entities.</summary>
		public virtual IEntityRelation GetSuperTypeRelation() { return null;}
		#endregion

		#region Included Code

		#endregion
	}
	
	/// <summary>Static class which is used for providing relationship instances which are re-used internally for syncing</summary>
	internal static class StaticPostGameRatingRelations
	{
		internal static readonly IEntityRelation GameEntityUsingGameIdStatic = new PostGameRatingRelations().GameEntityUsingGameId;
		internal static readonly IEntityRelation LeaguePlayerEntityUsingLeaguePlayerIdStatic = new PostGameRatingRelations().LeaguePlayerEntityUsingLeaguePlayerId;

		/// <summary>CTor</summary>
		static StaticPostGameRatingRelations()
		{
		}
	}
}
