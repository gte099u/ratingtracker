﻿///////////////////////////////////////////////////////////////
// This is generated code. 
//////////////////////////////////////////////////////////////
// Code is generated using LLBLGen Pro version: 4.2
// Code is generated on: 
// Code is generated using templates: SD.TemplateBindings.SharedTemplates
// Templates vendor: Solutions Design.
//////////////////////////////////////////////////////////////
using System;
using System.Collections;
using System.Data;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.DatabaseSpecific
{
	/// <summary>Singleton implementation of the PersistenceInfoProvider. This class is the singleton wrapper through which the actual instance is retrieved.</summary>
	/// <remarks>It uses a single instance of an internal class. The access isn't marked with locks as the PersistenceInfoProviderBase class is threadsafe.</remarks>
	internal static class PersistenceInfoProviderSingleton
	{
		#region Class Member Declarations
		private static readonly IPersistenceInfoProvider _providerInstance = new PersistenceInfoProviderCore();
		#endregion

		/// <summary>Dummy static constructor to make sure threadsafe initialization is performed.</summary>
		static PersistenceInfoProviderSingleton()
		{
		}

		/// <summary>Gets the singleton instance of the PersistenceInfoProviderCore</summary>
		/// <returns>Instance of the PersistenceInfoProvider.</returns>
		public static IPersistenceInfoProvider GetInstance()
		{
			return _providerInstance;
		}
	}

	/// <summary>Actual implementation of the PersistenceInfoProvider. Used by singleton wrapper.</summary>
	internal class PersistenceInfoProviderCore : PersistenceInfoProviderBase
	{
		/// <summary>Initializes a new instance of the <see cref="PersistenceInfoProviderCore"/> class.</summary>
		internal PersistenceInfoProviderCore()
		{
			Init();
		}

		/// <summary>Method which initializes the internal datastores with the structure of hierarchical types.</summary>
		private void Init()
		{
			this.InitClass(8);
			InitGameEntityMappings();
			InitLeagueEntityMappings();
			InitLeagueGameDetailEntityMappings();
			InitLeaguePlayerEntityMappings();
			InitLeaguePlayerDetailEntityMappings();
			InitPlayerEntityMappings();
			InitPostGameRatingEntityMappings();
			InitPostGameRatingDetailEntityMappings();
		}

		/// <summary>Inits GameEntity's mappings</summary>
		private void InitGameEntityMappings()
		{
			this.AddElementMapping("GameEntity", @"dbRatingTracker", @"dbo", "tblGame", 7, 0);
			this.AddElementFieldMapping("GameEntity", "Bias", "Bias", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 0);
			this.AddElementFieldMapping("GameEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("GameEntity", "LoserId", "LoserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("GameEntity", "LoserOriginalRating", "LoserOriginalRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
			this.AddElementFieldMapping("GameEntity", "TimeStamp", "TimeStamp", false, "DateTime", 0, 0, 0, false, "", new SD.LLBLGen.Pro.ORMSupportClasses.DateTimeDateTimeUTCConverter(), typeof(System.DateTime), 4);
			this.AddElementFieldMapping("GameEntity", "WinnerId", "WinnerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
			this.AddElementFieldMapping("GameEntity", "WinnerOriginalRating", "WinnerOriginalRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
		}

		/// <summary>Inits LeagueEntity's mappings</summary>
		private void InitLeagueEntityMappings()
		{
			this.AddElementMapping("LeagueEntity", @"dbRatingTracker", @"dbo", "tblLeague", 2, 0);
			this.AddElementFieldMapping("LeagueEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("LeagueEntity", "Name", "Name", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits LeagueGameDetailEntity's mappings</summary>
		private void InitLeagueGameDetailEntityMappings()
		{
			this.AddElementMapping("LeagueGameDetailEntity", @"dbRatingTracker", @"dbo", "vwLeagueGameDetail", 12, 4);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "Bias", "Bias", false, "Float", 0, 38, 0, false, "", null, typeof(System.Double), 0);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "GameId", "GameId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "LeagueId", "LeagueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "LoserId", "LoserId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "LoserName", "LoserName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "LoserOriginalRating", "LoserOriginalRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 5);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "LoserPlayerId", "LoserPlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 6);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "TimeStamp", "TimeStamp", false, "DateTime", 0, 0, 0, false, "", new SD.LLBLGen.Pro.ORMSupportClasses.DateTimeDateTimeUTCConverter(), typeof(System.DateTime), 7);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "WinnerId", "WinnerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 8);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "WinnerName", "WinnerName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 9);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "WinnerOriginalRating", "WinnerOriginalRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 10);
			this.AddElementFieldMapping("LeagueGameDetailEntity", "WinnerPlayerId", "WinnerPlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 11);
		}

		/// <summary>Inits LeaguePlayerEntity's mappings</summary>
		private void InitLeaguePlayerEntityMappings()
		{
			this.AddElementMapping("LeaguePlayerEntity", @"dbRatingTracker", @"dbo", "tblLeaguePlayer", 4, 0);
			this.AddElementFieldMapping("LeaguePlayerEntity", "CurrentRating", "CurrentRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("LeaguePlayerEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("LeaguePlayerEntity", "LeagueId", "LeagueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("LeaguePlayerEntity", "PlayerId", "PlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
		}

		/// <summary>Inits LeaguePlayerDetailEntity's mappings</summary>
		private void InitLeaguePlayerDetailEntityMappings()
		{
			this.AddElementMapping("LeaguePlayerDetailEntity", @"dbRatingTracker", @"dbo", "vwLeaguePlayerDetail", 6, 4);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "CurrentRating", "CurrentRating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 0);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "LeagueId", "LeagueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "LeagueName", "LeagueName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "LeaguePlayerId", "LeaguePlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "Name", "Name", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 4);
			this.AddElementFieldMapping("LeaguePlayerDetailEntity", "PlayerId", "PlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 5);
		}

		/// <summary>Inits PlayerEntity's mappings</summary>
		private void InitPlayerEntityMappings()
		{
			this.AddElementMapping("PlayerEntity", @"dbRatingTracker", @"dbo", "tblPlayer", 2, 0);
			this.AddElementFieldMapping("PlayerEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PlayerEntity", "Name", "Name", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 1);
		}

		/// <summary>Inits PostGameRatingEntity's mappings</summary>
		private void InitPostGameRatingEntityMappings()
		{
			this.AddElementMapping("PostGameRatingEntity", @"dbRatingTracker", @"dbo", "tblPostGameRating", 4, 0);
			this.AddElementFieldMapping("PostGameRatingEntity", "GameId", "GameId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PostGameRatingEntity", "Id", "Id", false, "Int", 0, 10, 0, true, "SCOPE_IDENTITY()", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PostGameRatingEntity", "LeaguePlayerId", "LeaguePlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 2);
			this.AddElementFieldMapping("PostGameRatingEntity", "Rating", "Rating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 3);
		}

		/// <summary>Inits PostGameRatingDetailEntity's mappings</summary>
		private void InitPostGameRatingDetailEntityMappings()
		{
			this.AddElementMapping("PostGameRatingDetailEntity", @"dbRatingTracker", @"dbo", "vwPostGameRatingDetail", 8, 4);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "GameId", "GameId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 0);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "LeagueId", "LeagueId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 1);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "LeagueName", "LeagueName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 2);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "LeaguePlayerId", "LeaguePlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 3);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "PlayerId", "PlayerId", false, "Int", 0, 10, 0, false, "", null, typeof(System.Int32), 4);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "PlayerName", "PlayerName", false, "VarChar", 150, 0, 0, false, "", null, typeof(System.String), 5);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "Rating", "Rating", false, "SmallInt", 0, 5, 0, false, "", null, typeof(System.Int16), 6);
			this.AddElementFieldMapping("PostGameRatingDetailEntity", "TimeStamp", "TimeStamp", false, "DateTime", 0, 0, 0, false, "", new SD.LLBLGen.Pro.ORMSupportClasses.DateTimeDateTimeUTCConverter(), typeof(System.DateTime), 7);
		}

	}
}
