﻿CREATE TABLE [dbo].[tblGame] (
    [Id]                   INT        IDENTITY (1, 1) NOT NULL,
    [WinnerId]             INT        NOT NULL,
    [LoserId]              INT        NOT NULL,
    [TimeStamp]            DATETIME   CONSTRAINT [DV_tblGame_TimeStamp] DEFAULT (getutcdate()) NOT NULL,
    [WinnerOriginalRating] SMALLINT   CONSTRAINT [DV_tblGame_WinnerOriginalRating] DEFAULT ((1200)) NOT NULL,
    [LoserOriginalRating]  SMALLINT   CONSTRAINT [DV_tblGame_LoserOriginalRating] DEFAULT ((1200)) NOT NULL,
    [Bias]                 FLOAT (53) CONSTRAINT [DV_tblGame_Bias] DEFAULT ((50)) NOT NULL,
    CONSTRAINT [PK_tblGame] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblGame_tblPlayer] FOREIGN KEY ([LoserId]) REFERENCES [dbo].[tblLeaguePlayer] ([Id]),
    CONSTRAINT [FK_tblGame_tblPlayer1] FOREIGN KEY ([WinnerId]) REFERENCES [dbo].[tblLeaguePlayer] ([Id])
);

