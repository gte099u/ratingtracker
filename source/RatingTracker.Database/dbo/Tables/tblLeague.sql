﻿CREATE TABLE [dbo].[tblLeague] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_tblLeague] PRIMARY KEY CLUSTERED ([Id] ASC)
);

