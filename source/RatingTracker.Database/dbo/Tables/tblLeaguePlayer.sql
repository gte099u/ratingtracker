﻿CREATE TABLE [dbo].[tblLeaguePlayer] (
    [Id]            INT      IDENTITY (1, 1) NOT NULL,
    [PlayerId]      INT      NOT NULL,
    [LeagueId]      INT      NOT NULL,
    [CurrentRating] SMALLINT NOT NULL,
    CONSTRAINT [PK_tblLeaguePlayer] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblLeaguePlayer_tblLeague] FOREIGN KEY ([LeagueId]) REFERENCES [dbo].[tblLeague] ([Id]),
    CONSTRAINT [FK_tblLeaguePlayer_tblPlayer] FOREIGN KEY ([PlayerId]) REFERENCES [dbo].[tblPlayer] ([Id]),
    CONSTRAINT [IX_tblLeaguePlayer] UNIQUE NONCLUSTERED ([LeagueId] ASC, [PlayerId] ASC)
);

