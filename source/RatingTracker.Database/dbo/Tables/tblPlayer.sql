﻿CREATE TABLE [dbo].[tblPlayer] (
    [Id]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (150) NOT NULL,
    CONSTRAINT [PK_tblPlayer] PRIMARY KEY CLUSTERED ([Id] ASC)
);

