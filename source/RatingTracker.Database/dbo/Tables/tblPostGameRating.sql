﻿CREATE TABLE [dbo].[tblPostGameRating] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [GameId]         INT      NOT NULL,
    [LeaguePlayerId] INT      NOT NULL,
    [Rating]         SMALLINT NOT NULL,
    CONSTRAINT [PK_tblPostGameRating] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_tblPostGameRating_tblGame] FOREIGN KEY ([GameId]) REFERENCES [dbo].[tblGame] ([Id]),
    CONSTRAINT [FK_tblPostGameRating_tblLeaguePlayer] FOREIGN KEY ([LeaguePlayerId]) REFERENCES [dbo].[tblLeaguePlayer] ([Id]),
    CONSTRAINT [IX_tblPostGameRating] UNIQUE NONCLUSTERED ([GameId] ASC, [LeaguePlayerId] ASC)
);

