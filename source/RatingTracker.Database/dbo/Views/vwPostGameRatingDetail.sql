﻿CREATE VIEW dbo.vwPostGameRatingDetail
AS
SELECT        dbo.tblPostGameRating.GameId, dbo.tblLeaguePlayer.PlayerId, dbo.tblPlayer.Name AS PlayerName, dbo.tblLeaguePlayer.LeagueId, dbo.tblPostGameRating.Rating, dbo.tblLeague.Name AS LeagueName, 
                         dbo.tblGame.TimeStamp, dbo.tblPostGameRating.LeaguePlayerId
FROM            dbo.tblPostGameRating INNER JOIN
                         dbo.tblLeaguePlayer ON dbo.tblPostGameRating.LeaguePlayerId = dbo.tblLeaguePlayer.Id INNER JOIN
                         dbo.tblPlayer ON dbo.tblLeaguePlayer.PlayerId = dbo.tblPlayer.Id INNER JOIN
                         dbo.tblLeague ON dbo.tblLeaguePlayer.LeagueId = dbo.tblLeague.Id INNER JOIN
                         dbo.tblGame ON dbo.tblPostGameRating.GameId = dbo.tblGame.Id

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[33] 4[28] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "tblPostGameRating"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 221
               Right = 281
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblLeaguePlayer"
            Begin Extent = 
               Top = 94
               Left = 644
               Bottom = 233
               Right = 834
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblPlayer"
            Begin Extent = 
               Top = 0
               Left = 994
               Bottom = 130
               Right = 1164
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblLeague"
            Begin Extent = 
               Top = 147
               Left = 1000
               Bottom = 256
               Right = 1170
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "tblGame"
            Begin Extent = 
               Top = 0
               Left = 377
               Bottom = 192
               Right = 580
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1950
         Alias = 2310
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or =', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostGameRatingDetail';






GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostGameRatingDetail';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'vwPostGameRatingDetail';



