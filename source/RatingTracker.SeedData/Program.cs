﻿using RatingTracker.Application;
using RatingTracker.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RatingTracker.SeedData
{
    class Program
    {
        private const int DAYS_OF_PLAY = 20;

        static void Main(string[] args)
        {

            var playerService = new PlayerService();
            var leagueService = new LeagueService();
            var gameService = new GameService();

            var leagueQuery = new LeagueQuery();
            var playerQuery = new PlayerQuery();

            PlayerEntity[] allPlayers;
            LeagueEntity[] allLeagues;

            // create players
            Console.WriteLine("Creating players");

            for (int i = 0; i < 20; i++)
            {
                var name = string.Format("Player_{0}", i);
                playerService.Create(name);
            }

            allPlayers = playerQuery.GetAll().ToArray();

            // create leagues
            Console.WriteLine("Creating leagues");

            for (int i = 0; i < 4; i++)
            {
                var name = string.Format("League_{0}", i);
                leagueService.Create(name);
            }

            allLeagues = leagueQuery.GetAll().ToArray();

            // add players to leagues
            Console.WriteLine("Adding players to leagues");
            for (int i = 0; i < allLeagues.Length; i++)
            {
                for (int j = 0; j < allPlayers.Length; j++)
                {
                    if (i == 0) // add all
                        leagueService.AddPlayer(allLeagues[i].Id, allPlayers[j].Id);

                    else if (j % 2 == 0) // add some
                        leagueService.AddPlayer(allLeagues[i].Id, allPlayers[j].Id);

                    else if (j % 3 == 0) // add some
                        leagueService.AddPlayer(allLeagues[i].Id, allPlayers[j].Id);

                    else if (j % 4 == 0) // add some
                        leagueService.AddPlayer(allLeagues[i].Id, allPlayers[j].Id);
                }
            }

            // play games
            var startDate = DateTime.Now;
            var leaguePlayers = playerQuery.GetAllLeaguePlayers();
            var rand = new Random();


            var groups = leaguePlayers.GroupBy(m => m.LeagueId).ToArray();

            for (int i = 0; i < groups.Count(); i++)
            {
                Console.WriteLine("Simulating games for league {0}", i + 1);

                var league = groups[i].Select(m => new
                {
                    Rating = (short)(1200 + (short)(300 * ((double)groups[i].ToList().FindIndex(n => n == m) / groups[i].Count()))),
                    Player = m
                }).ToArray();

                for (int q = 0; q < DAYS_OF_PLAY; q++)
                {
                    for (int j = 0; j < league.Count(); j++)
                    {
                        var player = league[j];

                        // create a game

                        var opponent = league.ElementAt(rand.Next(0, league.Count()));

                        while (opponent.Player.PlayerId == player.Player.PlayerId)
                        {
                            opponent = league.ElementAt(rand.Next(0, league.Count()));
                        }

                        var playerRating = player.Rating;
                        var opponentRating = opponent.Rating;

                        var calc = EloCalculator.GetBias(playerRating, opponentRating);


                        if (rand.NextDouble() >= calc)
                            gameService.Create(player.Player.LeaguePlayerId, opponent.Player.LeaguePlayerId, startDate);
                        else
                            gameService.Create(opponent.Player.LeaguePlayerId, player.Player.LeaguePlayerId, startDate);

                        startDate = startDate.AddMinutes(15);
                    }
                }
            }
        }
    }
}
