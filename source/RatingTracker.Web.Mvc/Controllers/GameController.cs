﻿using System;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using RatingTracker.Application;
using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using RatingTracker.Models.Game;
using SD.LLBLGen.Pro.ORMSupportClasses;


namespace RatingTracker.Controllers
{
    public class GameController : Controller
    {
        public ActionResult Index()
        {
            var vm = new IndexViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                var allGames = new EntityCollection<LeagueGameDetailEntity>();
                adapter.FetchEntityCollection(allGames, null);
                vm.Games = allGames.OrderBy(m => m.TimeStamp);
            }

            return View(vm);
        }

        [Authorize]
        public ActionResult Create(int id)
        {
            var playerQuery = new PlayerQuery();
            var league = new LeagueEntity(id);

            using (var adapter = new DataAccessAdapter())
            {
                adapter.FetchEntity(league);
            }

            var players = playerQuery.GetPlayersForLeague(id);

            var vm = new CreateViewModel
            {
                PlayerSelectList = new SelectList(players.OrderBy(m => m.Name), "LeaguePlayerId", "Name"),
                LeagueName = league.Name,
                LeagueId = id
            };

            return View(vm);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel form)
        {
            var service = new GameService();

            try
            {
                service.Create(form.SelectedWinnerId, form.SelectedLoserId);
                return RedirectToAction("Details", "League", new { id = form.LeagueId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            var vm = new DeleteViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                var toDelete = new LeagueGameDetailEntity(id);
                adapter.FetchEntity(toDelete);
                vm.Game = toDelete;
            }

            return View(vm);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Delete(DeleteViewModel form)
        {
            var service = new GameService();

            try
            {
                service.Delete(form.Id);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult Calculate()
        {
            return View();
        }


        [HttpPost]
        [Authorize]
        public ActionResult Calculate(CalculateViewModel form)
        {
            var service = new RatingCalculationService();

            try
            {
                service.RecalculateAllRatings();

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
    }
}