﻿using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using RatingTracker.Models.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RatingTracker.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var vm = new IndexViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                var games = new EntityCollection<LeagueGameDetailEntity>();
                adapter.FetchEntityCollection(games, null);
                vm.Games = games;

                var leagues = new EntityCollection<LeagueEntity>(); // all leagues are featured on homepage, for now
                adapter.FetchEntityCollection(leagues, null);
                vm.FeaturedLeagues = leagues;

                var leaguePlayers = new EntityCollection<LeaguePlayerDetailEntity>();
                adapter.FetchEntityCollection(leaguePlayers, null);
                vm.LeaguePlayers = leaguePlayers;
            }

            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}