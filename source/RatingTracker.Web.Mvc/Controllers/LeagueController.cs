﻿using System;
using System.Linq;
using System.Web.Mvc;
using RatingTracker.Application;
using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using RatingTracker.Models.League;
using SD.LLBLGen.Pro.ORMSupportClasses;

namespace RatingTracker.Controllers
{
    public class LeagueController : Controller
    {
        public ActionResult Index()
        {
            var vm = new IndexViewModel();

            var query = new LeagueQuery();
            vm.Leagues = query.GetAll();

            return View(vm);
        }

        public ActionResult Details(int id)
        {
            var vm = new DetailsViewModel { Id = id };

            using (var adapter = new DataAccessAdapter())
            {
                var league = new LeagueEntity(id);
                adapter.FetchEntity(league);
                vm.Name = league.Name;

                var players = new EntityCollection<LeaguePlayerDetailEntity>();
                var playerFilter = new RelationPredicateBucket(LeaguePlayerDetailFields.LeagueId == id);
                adapter.FetchEntityCollection(players, playerFilter);
                vm.Players = players;

                var games = new EntityCollection<LeagueGameDetailEntity>();
                var gameFilter = new RelationPredicateBucket(LeagueGameDetailFields.LeagueId == id);
                adapter.FetchEntityCollection(games, gameFilter);
                vm.Games = games;
            }

            return View(vm);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult History(int id)
        {
            var vm = new HistoryViewModel { LeagueId = id };
            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel form)
        {
            try
            {
                var service = new LeagueService();
                service.Create(form.Name);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult AddPlayer(int id)
        {
            var vm = new AddPlayerViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                // for now, getting all players - update this to exclude players already in league
                var eligiblePlayers = new EntityCollection<PlayerEntity>();
                adapter.FetchEntityCollection(eligiblePlayers, null);
                var league = new LeagueEntity(id);
                adapter.FetchEntity(league);
                vm.Players = new SelectList(eligiblePlayers.OrderBy(m => m.Name), "Id", "Name");
                vm.LeagueId = id;
                vm.LeagueName = league.Name;
            }

            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPlayer(AddPlayerViewModel form)
        {
            try
            {
                var service = new LeagueService();

                service.AddPlayer(form.LeagueId, form.SelectedPlayerId);

                return RedirectToAction("Details", new { id = form.LeagueId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult RemovePlayer(int id)
        {
            var vm = new RemovePlayerViewModel { ToRemoveId = id };

            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemovePlayer(RemovePlayerViewModel form)
        {
            try
            {
                var service = new LeagueService();
                service.RemovePlayer(form.ToRemoveId);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
    }
}