﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using RatingTracker.DatabaseSpecific;
using RatingTracker.EntityClasses;
using RatingTracker.HelperClasses;
using RatingTracker.Models.Player;
using SD.LLBLGen.Pro.ORMSupportClasses;
using RatingTracker.Application;

namespace RatingTracker.Controllers
{
    public class PlayerController : Controller
    {
        public ActionResult Index()
        {
            var query = new PlayerQuery();
            var vm = new IndexViewModel();
            vm.Players = query.GetAll();

            return View(vm);
        }

        public ActionResult Profile(int id)
        {
            var vm = new ProfileViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                var playerGames = new EntityCollection<LeagueGameDetailEntity>();
                var filter = new RelationPredicateBucket(LeagueGameDetailFields.LoserPlayerId == id | LeagueGameDetailFields.WinnerPlayerId == id);

                adapter.FetchEntityCollection(playerGames, filter);
                vm.Games = playerGames;
                var player = new PlayerEntity(id);
                adapter.FetchEntity(player);
                vm.Player = player;

                var leagues = new EntityCollection<LeaguePlayerDetailEntity>();
                var leagueFilter = new RelationPredicateBucket(LeaguePlayerDetailFields.PlayerId == id);
                adapter.FetchEntityCollection(leagues, leagueFilter);
                vm.Leagues = leagues;
            }

            return View(vm);
        }

        public ActionResult League(int id)
        {
            var vm = new LeagueViewModel();

            using (var adapter = new DataAccessAdapter())
            {
                var playerGames = new EntityCollection<LeagueGameDetailEntity>();
                var filter = new RelationPredicateBucket(LeagueGameDetailFields.LoserId == id | LeagueGameDetailFields.WinnerId == id);
                adapter.FetchEntityCollection(playerGames, filter);
                vm.Games = playerGames;

                var leaguePlayer = new LeaguePlayerEntity(id);
                adapter.FetchEntity(leaguePlayer);
                vm.LeaguePlayer = leaguePlayer;

                var player = new PlayerEntity(leaguePlayer.PlayerId);
                adapter.FetchEntity(player);
                vm.Player = player;

                var postGameRatings = new EntityCollection<PostGameRatingDetailEntity>();
                var ratingFilter = new RelationPredicateBucket(PostGameRatingDetailFields.LeaguePlayerId == id);
                adapter.FetchEntityCollection(postGameRatings, ratingFilter);
                vm.MaxRating = postGameRatings.Max(m => m.Rating);
            }

            return View(vm);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateViewModel form)
        {
            try
            {
                var service = new PlayerService();
                service.Create(form.Name);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [Authorize]
        public ActionResult Edit(int id)
        {
            var query = new PlayerQuery();
            var player = query.Get(id);
            var vm = new EditViewModel
            {
                Name = player.Name,
                Id = id
            };

            return View(vm);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditViewModel form)
        {
            try
            {
                var query = new PlayerQuery();
                var service = new PlayerService();

                var player = query.Get(form.Id);

                player.Name = form.Name;

                service.Save(player);
                return RedirectToAction("Profile", new { form.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public JsonResult RatingsOverTime(int id)
        {
            using (var adapter = new DataAccessAdapter())
            {
                var ratings = new EntityCollection<PostGameRatingDetailEntity>();
                var filter = new RelationPredicateBucket(PostGameRatingDetailFields.LeagueId == id);
                adapter.FetchEntityCollection(ratings, filter);

                var players = ratings.GroupBy(m => m.PlayerId);

                var result = new List<RatingResult>();

                foreach (var player in players)
                {
                    var toAdd = new RatingResult
                    {
                        PlayerName = player.First().PlayerName,
                        TimeRating = player
                            .OrderByDescending(m => m.TimeStamp)
                            .Take(100)
                            .OrderBy(m => m.TimeStamp)
                            .Select(m => new Tuple<string, short>(m.TimeStamp.ToString("M/d/y h:m:s"), m.Rating))
                            .ToArray()
                    };
                    result.Add(toAdd);
                }

                var toRet = result.Select(m => new
                {
                    name = m.PlayerName,
                    data = m.TimeRating.Select(n => new
                    {
                        x = n.Item1,
                        y = n.Item2
                    })
                });

                return Json(toRet.ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

        [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
        public JsonResult Record(int id)
        {
            var games = new EntityCollection<GameEntity>();
            var filter = new RelationPredicateBucket(GameFields.WinnerId == id | GameFields.LoserId == id);

            using (var adapter = new DataAccessAdapter())
            {
                adapter.FetchEntityCollection(games, filter);
            }

            var toRet = games.OrderBy(m => m.TimeStamp).Select(m => new
            {
                val = m.WinnerId == id ? 1 : -1
            });

            return Json(toRet.Take(100).Select(m => m.val).ToArray(), JsonRequestBehavior.AllowGet);
        }

        public static double DateTimeToUnixTimestamp(DateTime dateTime)
        {
            return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
        }

        private class RatingResult
        {
            public string PlayerName { get; set; }
            public IEnumerable<Tuple<string, short>> TimeRating { get; set; }
        }

        //public ActionResult Edit(int id)
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Edit(EditViewModel form)
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        //public ActionResult Delete(int id)
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Delete(DeleteViewModel form)
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}