﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RatingTracker.Models.Game
{
    public class CreateViewModel
    {
        public string LeagueName { get; set; }
        public int LeagueId { get; set; }
        public int SelectedWinnerId { get; set; }
        public int SelectedLoserId { get; set; }
        public SelectList PlayerSelectList { get; set; }
    }
}