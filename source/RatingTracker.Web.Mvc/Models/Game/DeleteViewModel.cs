﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.Game
{
    public class DeleteViewModel
    {
        public int Id { get; set; }
        public LeagueGameDetailEntity Game { get; set; }

    }
}