﻿using System.Collections.Generic;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.Game
{
    public class IndexViewModel
    {
        public IEnumerable<LeagueGameDetailEntity> Games { get; set; }        
    }
}