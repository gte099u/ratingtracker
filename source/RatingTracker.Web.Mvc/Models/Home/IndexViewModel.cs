﻿using RatingTracker.EntityClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RatingTracker.Models.Home
{
    public class IndexViewModel
    {
        public int[] LeagueIds { get; set; }
        public IEnumerable<LeagueGameDetailEntity> Games { get; set; }
        public IEnumerable<LeagueEntity> FeaturedLeagues { get; set; }
        public IEnumerable<LeaguePlayerDetailEntity> LeaguePlayers { get; set; }
        public IEnumerable<LeaguePlayerDetailEntity> GetPlayers (int leaugeId)
        {
            return LeaguePlayers.Where(m => m.LeagueId == leaugeId);
        }
    }
}