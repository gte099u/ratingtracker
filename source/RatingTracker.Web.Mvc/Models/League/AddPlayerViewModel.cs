﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;
using System.Web.Mvc;

namespace RatingTracker.Models.League
{
    public class AddPlayerViewModel
    {
        public SelectList Players { get; set; }
        public int SelectedPlayerId { get; set; }
        public int LeagueId { get; set; }
        public string LeagueName { get; set; }
    }
}