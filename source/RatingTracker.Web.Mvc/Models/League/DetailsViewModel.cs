﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.League
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<LeagueGameDetailEntity> Games { get; set; }
        public IEnumerable<LeaguePlayerDetailEntity> Players { get; set; }
    }
}