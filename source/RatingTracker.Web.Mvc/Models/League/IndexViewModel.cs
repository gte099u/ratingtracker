﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.League
{
    public class IndexViewModel
    {
        public IEnumerable<LeagueEntity> Leagues { get; set; }
    }
}