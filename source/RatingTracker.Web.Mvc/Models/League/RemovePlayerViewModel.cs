﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;
using System.Web.Mvc;

namespace RatingTracker.Models.League
{
    public class RemovePlayerViewModel
    {
        public int ToRemoveId { get; set; }
    }
}