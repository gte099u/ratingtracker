﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RatingTracker.Models.Player
{
    public class CreateViewModel
    {
        [StringLength(150)]
        public string Name { get; set; }
    }
}