﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RatingTracker.Models.Player
{
    public class EditViewModel
    {
        [StringLength(150)]
        public string Name { get; set; }
        public int Id { get; set; }
    }
}