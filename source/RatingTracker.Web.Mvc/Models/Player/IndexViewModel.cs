﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.Player
{
    public class IndexViewModel
    {
        public IEnumerable<PlayerEntity> Players { get; set; }
    }
}