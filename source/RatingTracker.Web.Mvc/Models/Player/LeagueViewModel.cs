﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.Player
{
    public class LeagueViewModel
    {
        public IEnumerable<LeagueGameDetailEntity> Games { get; set; }
        public PlayerEntity Player { get; set; }
        public LeaguePlayerEntity LeaguePlayer { get; set; }

        public int MaxRating { get; set; }
    }
}