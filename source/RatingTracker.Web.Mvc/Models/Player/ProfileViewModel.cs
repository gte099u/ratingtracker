﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RatingTracker.EntityClasses;

namespace RatingTracker.Models.Player
{
    public class ProfileViewModel
    {
        public IEnumerable<LeagueGameDetailEntity> Games { get; set; }
        public PlayerEntity Player { get; set; }

        public IEnumerable<LeaguePlayerDetailEntity> Leagues { get; set; }
    }
}