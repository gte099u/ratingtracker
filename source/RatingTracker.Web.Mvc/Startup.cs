﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RatingTracker.Startup))]
namespace RatingTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
